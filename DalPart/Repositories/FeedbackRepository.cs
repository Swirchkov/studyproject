﻿using System;
using System.Collections.Generic;
using System.Linq;
using DalPart.Models;
using System.Data.Entity;

namespace DalPart.Repositories
{
    internal class FeedbackRepository : Repository, IRepository<Feedback> 
    {
        public FeedbackRepository() : base() { }

        public void Add(Feedback item)
        {
            db.Feedbacks.Add(item);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Feedback dbItem = db.Feedbacks.FirstOrDefault(fb => fb.FeedbackId == id);

            if (dbItem == null)
            {
                throw new ArgumentException("Invalid id");
            }

            db.Feedbacks.Remove(dbItem);
            db.SaveChanges();
        }

        public IEnumerable<Feedback> Find(Func<Feedback, bool> predicate) => db.Feedbacks.Where(predicate);

        public void Update(Feedback item)
        {
            Feedback dbItem = db.Feedbacks.FirstOrDefault(fb => fb.FeedbackId == item.FeedbackId);

            if (dbItem == null)
            {
                throw new ArgumentException("Invalid id");
            }

            dbItem.Processed = item.Processed;
            dbItem.Text = item.Text;
            dbItem.Title = item.Title;

            db.Entry(dbItem).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
