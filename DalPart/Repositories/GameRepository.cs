﻿using DalPart.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace DalPart.Repositories
{
    internal class GameRepository : Repository, IRepository<Game>
    {
        public GameRepository() : base()
        {
        }

        public void Add(Game item)
        {
            db.Games.Add(item);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Game item = db.Games.FirstOrDefault(g => g.GameId == id);

            if (item == null)
            {
                throw new ArgumentException("Invalid id");
            }

            db.Games.Remove(item);
            db.SaveChanges();
        }

        public IEnumerable<Game> Find(Func<Game, bool> predicate) => db.Games.Where(predicate);

        public void Update(Game item)
        {
            Game dbItem = db.Games.FirstOrDefault(g => g.GameId == item.GameId);

            if (dbItem == null)
            {
                throw new ArgumentException("Invalid id");
            }

            dbItem.FirstScore = item.FirstScore;
            dbItem.SecondScore = item.SecondScore;
            dbItem.User1Id = item.User1Id;
            dbItem.User2Id = item.User2Id;
            dbItem.GameInfoId = item.GameInfoId;

            db.Entry(dbItem).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
