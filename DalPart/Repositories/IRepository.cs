﻿using System;
using System.Collections.Generic;

namespace DalPart.Repositories
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> Find(Func<T, bool> predicate);
        void Add(T item);
        void Update(T item);
        void Delete(int id);
    }
}
