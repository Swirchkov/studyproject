﻿using System;
using System.Collections.Generic;
using System.Linq;
using DalPart.Models;
using System.Data.Entity;

namespace DalPart.Repositories
{
    internal class GameInfoRepository : Repository, IRepository<GameInfo>
    {
        public GameInfoRepository() : base()
        {
        }

        public void Add(GameInfo item)
        {
            db.Infos.Add(item);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            GameInfo item = db.Infos.FirstOrDefault(g => g.Id == id);

            if (item == null)
            {
                throw new ArgumentException("Invalid id");
            }

            db.Infos.Remove(item);
            db.SaveChanges();
        }

        public IEnumerable<GameInfo> Find(Func<GameInfo, bool> predicate) => db.Infos.Where(predicate);

        public void Update(GameInfo item)
        {
            GameInfo info = db.Infos.FirstOrDefault(gi => gi.Id == item.Id);

            if (info == null)
            {
                throw new ArgumentException("Invalid id");
            }

            info.Name = item.Name;

            db.Entry(info).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
