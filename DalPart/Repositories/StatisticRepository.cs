﻿using System;
using System.Collections.Generic;
using System.Linq;
using DalPart.Models;
using System.Data.Entity;

namespace DalPart.Repositories
{
    internal class StatisticRepository : Repository, IRepository<Statistic>
    {
        public StatisticRepository() : base() { }

        public void Add(Statistic item)
        {
            db.Statistics.Add(item);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Statistic item = db.Statistics.FirstOrDefault(st => st.StatisticId == id);

            if (item == null)
            {
                throw new ArgumentException("Invalid id");
            }

            db.Statistics.Remove(item);
            db.SaveChanges();
        }

        public IEnumerable<Statistic> Find(Func<Statistic, bool> predicate) => db.Statistics.Where(predicate);

        public void Update(Statistic item)
        {
            Statistic dbItem = db.Statistics.FirstOrDefault(st => st.StatisticId == item.StatisticId);

            if (dbItem == null)
            {
                throw new ArgumentException("Invalid id");
            }

            dbItem.DrawsII = item.DrawsII;
            dbItem.LosesII = item.LosesII;
            dbItem.WinsII = item.WinsII;

            dbItem.RealDraws = item.RealDraws;
            dbItem.RealLoses = item.RealLoses;
            dbItem.RealWins = item.RealWins;

            dbItem.InfoId = item.InfoId;
            dbItem.UserId = item.UserId;

            db.Entry(dbItem).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
