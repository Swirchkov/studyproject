﻿using DalPart.Models;

namespace DalPart.Repositories
{
    internal abstract class Repository
    {
        private static GameContext singleContext; // single game context used by all repositories
        protected GameContext db;

        static Repository()
        {
            singleContext = GameContext.CreateInstance();
        }

        internal Repository()
        {
            this.db = singleContext;
        }
    }
}
