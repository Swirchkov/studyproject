﻿using System;
using System.Collections.Generic;
using System.Linq;
using DalPart.Models;
using System.Data.Entity;

namespace DalPart.Repositories
{
    internal class UserRepository : Repository, IRepository<User>
    {
        public UserRepository() : base() { }

        public void Add(User item)
        {
            db.Users.Add(item);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            User dbItem = db.Users.FirstOrDefault(u => u.UserId == id);

            if (dbItem == null)
            {
                throw new ArgumentException("Invalid id");
            }

            db.Users.Remove(dbItem);
            db.SaveChanges();
        }

        public IEnumerable<User> Find(Func<User, bool> predicate) => db.Users.Where(predicate);

        public void Update(User item)
        {
            User dbItem = db.Users.Find(item.UserId);   // search by id

            if (dbItem == null)
            {
                throw new ArgumentException("Invalid id");
            }

            dbItem.Avatar = item.Avatar;
            dbItem.DisplayName = item.DisplayName;
            dbItem.Email = item.Email;
            dbItem.IsEmailVerified = item.IsEmailVerified;
            dbItem.Login = item.Login;
            dbItem.Password = item.Password;

            db.Entry(dbItem).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
