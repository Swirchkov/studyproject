namespace DalPart.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixingrelationshipbetweenuserandstatistics : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Statistics", "StatisticId", "dbo.Users");
            //DropColumn("dbo.Statistics", "UserId");
            //RenameColumn(table: "dbo.Statistics", name: "StatisticId", newName: "UserId");
            RenameIndex(table: "dbo.Statistics", name: "IX_StatisticId", newName: "IX_UserId");
            DropPrimaryKey("dbo.Statistics");
            AlterColumn("dbo.Statistics", "StatisticId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Statistics", "StatisticId");
            AddForeignKey("dbo.Statistics", "UserId", "dbo.Users", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Statistics", "UserId", "dbo.Users");
            DropPrimaryKey("dbo.Statistics");
            AlterColumn("dbo.Statistics", "StatisticId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Statistics", "StatisticId");
            RenameIndex(table: "dbo.Statistics", name: "IX_UserId", newName: "IX_StatisticId");
            RenameColumn(table: "dbo.Statistics", name: "UserId", newName: "StatisticId");
            AddColumn("dbo.Statistics", "UserId", c => c.Int(nullable: false));
            AddForeignKey("dbo.Statistics", "StatisticId", "dbo.Users", "UserId");
        }
    }
}
