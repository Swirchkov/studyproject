namespace DalPart.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        FeedbackId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Text = c.String(),
                        Processed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.FeedbackId);
            
            CreateTable(
                "dbo.Games",
                c => new
                    {
                        GameId = c.Int(nullable: false, identity: true),
                        User1Id = c.Int(nullable: false),
                        User2Id = c.Int(),
                        FirstScore = c.Int(nullable: false),
                        SecondScore = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GameId)
                .ForeignKey("dbo.Users", t => t.User2Id)
                .ForeignKey("dbo.Users", t => t.User1Id, cascadeDelete: true)
                .Index(t => t.User1Id)
                .Index(t => t.User2Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        DisplayName = c.String(),
                        Avatar = c.String(),
                        IsEmailVerified = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Statistics",
                c => new
                    {
                        InfoId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        StatisticId = c.Int(nullable: false),
                        WinsII = c.Int(nullable: false),
                        DrawsII = c.Int(nullable: false),
                        LosesII = c.Int(nullable: false),
                        RealWins = c.Int(nullable: false),
                        RealDraws = c.Int(nullable: false),
                        RealLoses = c.Int(nullable: false),
                        User_UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.InfoId, t.UserId })
                .ForeignKey("dbo.GameInfoes", t => t.InfoId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.InfoId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.GameInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Statistics", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Statistics", "InfoId", "dbo.GameInfoes");
            DropForeignKey("dbo.Games", "User1Id", "dbo.Users");
            DropForeignKey("dbo.Games", "User2Id", "dbo.Users");
            DropIndex("dbo.Statistics", new[] { "User_UserId" });
            DropIndex("dbo.Statistics", new[] { "InfoId" });
            DropIndex("dbo.Games", new[] { "User2Id" });
            DropIndex("dbo.Games", new[] { "User1Id" });
            DropTable("dbo.GameInfoes");
            DropTable("dbo.Statistics");
            DropTable("dbo.Users");
            DropTable("dbo.Games");
            DropTable("dbo.Feedbacks");
        }
    }
}
