namespace DalPart.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relationshipbetweengameandgameinfos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Games", "GameInfoId", c => c.Int(nullable: false));
            CreateIndex("dbo.Games", "GameInfoId");
            AddForeignKey("dbo.Games", "GameInfoId", "dbo.GameInfoes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Games", "GameInfoId", "dbo.GameInfoes");
            DropIndex("dbo.Games", new[] { "GameInfoId" });
            DropColumn("dbo.Games", "GameInfoId");
        }
    }
}
