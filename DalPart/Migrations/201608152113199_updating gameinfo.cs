namespace DalPart.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatinggameinfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GameInfoes", "Description", c => c.String());
            AddColumn("dbo.GameInfoes", "Avatar", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GameInfoes", "Avatar");
            DropColumn("dbo.GameInfoes", "Description");
        }
    }
}
