namespace DalPart.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnServerNametoGameInfotable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.GameInfoes", "ServerName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.GameInfoes", "ServerName");
        }
    }
}
