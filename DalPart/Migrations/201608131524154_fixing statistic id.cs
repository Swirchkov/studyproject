namespace DalPart.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixingstatisticid : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Statistics", "StatisticId");
            RenameColumn(table: "dbo.Statistics", name: "User_UserId", newName: "StatisticId");
            RenameIndex(table: "dbo.Statistics", name: "IX_User_UserId", newName: "IX_StatisticId");
            DropPrimaryKey("dbo.Statistics");
            AddPrimaryKey("dbo.Statistics", "StatisticId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Statistics");
            AddPrimaryKey("dbo.Statistics", new[] { "InfoId", "UserId" });
            RenameIndex(table: "dbo.Statistics", name: "IX_StatisticId", newName: "IX_User_UserId");
            RenameColumn(table: "dbo.Statistics", name: "StatisticId", newName: "User_UserId");
            AddColumn("dbo.Statistics", "StatisticId", c => c.Int(nullable: false));
        }
    }
}
