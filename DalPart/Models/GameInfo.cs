﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalPart.Models
{
    public class GameInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ServerName { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }

        public virtual ICollection<Statistic> Statistics { get; set; }
        public virtual ICollection<Game> Games { get; set; }
    }
}
