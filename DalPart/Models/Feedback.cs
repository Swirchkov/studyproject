﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalPart.Models
{
    public class Feedback
    {
        public int FeedbackId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public bool Processed { get; set; }
    }
}
