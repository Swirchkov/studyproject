﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalPart.Models
{
    public class Statistic
    {
        public int StatisticId { get; set; }
        public int UserId { get; set; }
        public int InfoId { get; set; }

        // games with inteligence intellect

        public int WinsII { get; set; }
        public int DrawsII { get; set; }
        public int LosesII { get; set; }

        // games with real players

        public int RealWins { get; set; }
        public int RealDraws { get; set; }
        public int RealLoses { get; set; }

        public virtual User User { get; set; }
        public virtual GameInfo Info { get; set; }

    }
}
