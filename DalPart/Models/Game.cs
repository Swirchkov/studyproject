﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DalPart.Models
{
    public class Game
    {
        public int GameId { get; set; }
        public int User1Id { get; set; }
        public  int? User2Id { get; set; }
        public int FirstScore { get; set; }
        public int SecondScore { get; set; }
        public int GameInfoId { get; set; }

        public virtual User User1 { get; set; }
        public virtual User User2 { get; set; }
        public virtual GameInfo GameInfo { get; set; }
    }
}
