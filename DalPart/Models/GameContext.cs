﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DalPart.Models
{
    internal class GameContext : DbContext
    {
        public GameContext() : base("name=Default")
        {

        }

        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameInfo> Infos { get; set; }
        public DbSet<Statistic> Statistics { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // keys
            modelBuilder.Entity<Feedback>().HasKey(f => f.FeedbackId);
            modelBuilder.Entity<Game>().HasKey(g => g.GameId);
            modelBuilder.Entity<GameInfo>().HasKey(info => info.Id);
            modelBuilder.Entity<Statistic>().HasKey(st => st.StatisticId);
            modelBuilder.Entity<User>().HasKey(u => u.UserId);

            // foreign keys
            modelBuilder.Entity<User>().HasMany(u => u.HomeGames).WithRequired(g => g.User1).HasForeignKey(g => g.User1Id);
            modelBuilder.Entity<User>().HasMany(u => u.AbroadGames).WithOptional(g => g.User2).HasForeignKey(g => g.User2Id);

            modelBuilder.Entity<Statistic>().HasRequired(s => s.Info).WithMany(info => info.Statistics);
            modelBuilder.Entity<Statistic>().HasRequired(s => s.User).WithMany(u => u.Statistics).HasForeignKey(st => st.UserId);
            modelBuilder.Entity<Game>().HasRequired(g => g.GameInfo).WithMany(info => info.Games);

            base.OnModelCreating(modelBuilder);
        }

        private static GameContext context;

        public static GameContext CreateInstance()
        {
            if (context == null)
            {
                 context = new GameContext();
            }
            return context;
        }
    }
}
