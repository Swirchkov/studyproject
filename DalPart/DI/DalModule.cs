﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using DalPart.Repositories;
using DalPart.Models;

namespace DalPart.DI
{
    public class DalModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GameInfoRepository>().As<IRepository<GameInfo>>();
            builder.RegisterType<FeedbackRepository>().As<IRepository<Feedback>>();
            builder.RegisterType<GameRepository>().As<IRepository<Game>>();
            builder.RegisterType<StatisticRepository>().As<IRepository<Statistic>>();
            builder.RegisterType<UserRepository>().As<IRepository<User>>();

            base.Load(builder);
        }
    }
}
