﻿using BllPart.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPart.Filters
{
    public class AuthorizeFilter : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            UserDTO user = (UserDTO)filterContext.HttpContext.Session["User"];

            if (user == null) 
            {
                filterContext.Result = new RedirectResult("/Account/Login");
            }
        }
    }
}