﻿var Help = function () {

    var loader = new DynamicLoader();

    var helpObj = null;

    loader.getData("/json/help.json").success(function (data) {
        helpObj = data;
    });

    function renderMessage(message) {

        $("#ruleView").html(message);
    };

    this.getHelpMessage = function (key) {

        if (helpObj == null) {
            return null;
        }
        else {
            return helpObj[key];
        }

    };

    this.displayHelpMessage = function (key) {
        var message = this.getHelpMessage(key);

        if (message == null) {
            var self = this;

            var timer = setInterval(function () {
                message = self.getHelpMessage(key);

                if (message != null) {

                    clearInterval(timer);
                    renderMessage(message);

                }

            }, 100);
        }
        else {
            renderMessage(message);
        }
    };

}