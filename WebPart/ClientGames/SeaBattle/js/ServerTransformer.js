﻿var ServerTransformer = function(deltaX, deltaY, xDimensions, yDimensions) {

    const EMPTY = 0, HEALTH = 3;

    function generateZeroFilledArray(length) {
        length = +length;

        if (isNaN(length) || length < 0) {
            throw new Error("Invalid array length;");
        }

        var arr = [];

        for (var i = 0; i < length; i++) {
            arr.push(EMPTY);
        }

        return arr;
    };

    function detectXCoordinate(p) {

        if (p === null || p === undefined) {
            throw new Error('null or undefined point');
        }

        return p.X / deltaX;
    };

    function detectYCoordinate(p) {

        if (p === null || p === undefined) {
            throw new Error('null or undefined point');
        }

        return p.Y / deltaY;
    };

    this.transformForServer = function (fleet) {
        var matrix = [];

        for (var i = 0; i < yDimensions; i++) {
            matrix.push(generateZeroFilledArray(xDimensions));
        }

        console.log(matrix);

        var ships = fleet.getShips();

        for (var i = 0; i < ships.length; i++) {
            var ship = ships[i];

            var rectangles = ship.getRectangles();

            for (var j = 0; j < rectangles.length; j++) {
                var rectangle = rectangles[j];

                var pLeftTop = rectangle.getLeftTopPoint();
                var x = detectXCoordinate(pLeftTop);
                var y = detectYCoordinate(pLeftTop);

                matrix[y][x] = HEALTH;
            }
        }

        window.lastFleet = matrix;

        return matrix;
    };

    this.stepToRectangle = function (stepX, stepY) {

        var rectangle = new Rectangle(new Point(stepX * deltaX, stepY * deltaY), 
            new Point((stepX + 1) * deltaX, (stepY + 1) * deltaY));

        return rectangle;
    };

    this.rectangleToStep = function (rect) {

        if (rect === undefined || rect === null) {
            throw new Error("null or undefined rectangle argument");
        }

        var pLeftTop = rect.getLeftTopPoint();

        return {
            X: Math.floor(pLeftTop.X / deltaX),
            Y : Math.floor(pLeftTop.Y / deltaY)
        };
    };

    this.getLastFleet = function () {
        return window.lastFleet;
    };

};