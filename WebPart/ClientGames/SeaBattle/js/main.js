﻿window.ongamehtmlloaded =  function () {

    window.UserGUI = userGUI();

    window.UserGUI.drawGrid();

    window.opponentGUI = new OpponentGUI();
    window.opponentGUI.drawGrid();

    var receiver = new GUIReceiver();
    receiver.Initialize();

    var help = new Help();

    help.displayHelpMessage("placeShips");
};