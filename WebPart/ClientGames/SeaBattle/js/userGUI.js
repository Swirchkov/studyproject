﻿var userGUI = function () {
    var obj = {};

    var canvas = document.getElementById("userCanvas");
    var context = canvas.getContext("2d");

    var fleet = new Fleet();

    function updateStartBtn() {

        var config = new Config();

        var startBtn = $("#startBtn");

        if (config.checkFleetToStart(fleet)) {

            if (startBtn.hasClass('disabled')) {
                startBtn.removeClass('disabled');
            }

        }
        else {

            if (!startBtn.hasClass('disabled')) {
                startBtn.addClass('disabled');
            }

        }

    };

    function isNumber(num) {
        num = +num;

        if (isNaN(num)) {
            throw new Error("invalid nummber");
        }

        return true;
    };

    obj.drawGrid = function () {
        var gridHelper = new GridHelper(canvas.width, canvas.height);
        gridHelper.drawGrid(canvas, context);
    };

    obj.mouseClick = function (p) {
        var canvasHelper = new CanvasHelper(canvas, context);
        var gridHelper = new GridHelper(canvas.width, canvas.height);

        var rectangle = gridHelper.getPointRectangle(p);

        if (fleet.addRectangle(rectangle)) {

            canvasHelper.clearCnavas();

            this.drawGrid();
            canvasHelper.drawFleet(fleet);

            updateStartBtn();

            $("#programView").html(fleet.toString());

        }
        else {
            $("#programView").html("Невозможно прибавить к какому-либо кораблю даный квадрат");
        }

    };

    obj.prepareServerObject = function () {
        var gridhelper = new GridHelper(canvas.width, canvas.height);

        var transformer = new ServerTransformer(gridhelper.getDeltaX(), gridhelper.getDeltaY(),
            gridhelper.getVerticalDimensions(), gridhelper.getHorizontalDimensions());

        return { Field: transformer.transformForServer(fleet), StepX: 0, StepY : 0 };
    };

    obj.getRectangleForStep = function (stepX, stepY) {

        if (!isNumber(stepX) || !isNumber(stepY)) {
            return;
        }

        var gridhelper = new GridHelper(canvas.width, canvas.height);

        var transformer = new ServerTransformer(gridhelper.getDeltaX(), gridhelper.getDeltaY(),
            gridhelper.getVerticalDimensions(), gridhelper.getHorizontalDimensions());

        return transformer.stepToRectangle(stepX, stepY);
    };

    obj.isShipHit = function (rectangle) {

        var ships = fleet.getShips();

        for (var i = 0; i < ships.length; i++) {
            var ship = ships[i];

            if (ship.hasRectangle(rectangle)) {
                return true;
            }
        }

        return false;
    };

    return obj;
};

