﻿var OpponentGUI = function () {

    var canvas = document.getElementById("opponentCanvas");
    var context = canvas.getContext("2d");

    var lastStepRectangle;
    var shipHitRectangles = [];

    function makeStep(rectangle) {

        var server = new Server();

        var gridhelper = new GridHelper(canvas.width, canvas.height);

        var transformer = new ServerTransformer(gridhelper.getDeltaX(), gridhelper.getDeltaY(),
            gridhelper.getVerticalDimensions(), gridhelper.getHorizontalDimensions());

        console.log(transformer);

        var step = transformer.rectangleToStep(rectangle);

        server.MakeStep({ StepX: step.X, StepY: step.Y, Field: transformer.getLastFleet() });

    };

    this.drawGrid = function () {
        var gridHelper = new GridHelper(canvas.width, canvas.height);
        gridHelper.drawGrid(canvas, context);
    };

    this.mouseClick = function (p) {

        var gridHelper = new GridHelper(canvas.width, canvas.height);

        var rectangle = gridHelper.getPointRectangle(p);

        var canvasHelper = new CanvasHelper(canvas, canvas.getContext('2d'));
        canvasHelper.drawRedCircle

        lastStepRectangle = rectangle;

        makeStep(rectangle);
        $("#programView").html("Ожидайте оппонента.");
    };

    this.drawLastHit = function () {

        if (lastStepRectangle === undefined) {
            return;
        }

        var canvasHelper = new CanvasHelper(canvas, context);
        canvasHelper.drawHitRectangle(lastStepRectangle);

        shipHitRectangles.push(lastStepRectangle);
    };

    this.drawLastMiss = function () {
        if (lastStepRectangle === undefined) {
            return;
        }

        var canvasHelper = new CanvasHelper(canvas, context);
        canvasHelper.drawMiss(lastStepRectangle);
    };

    this.shipHit = function () {

        var canvasHelper = new CanvasHelper(canvas, context);

        for (var i = 0; i < shipHitRectangles.length; i++) {
            canvasHelper.drawFinalHit(shipHitRectangles[i]);
        }

        shipHitRectangles = [];
    };
}