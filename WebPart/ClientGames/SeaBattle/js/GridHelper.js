﻿var GridHelper = function (width, height) {
    
    var horDimension = 8, verDimension = 8;

    this.getHorizontalDimensions = function () {
        return horDimension;
    };

    this.getVerticalDimensions = function () {
        return verDimension;
    };

    this.getHorizontalLines = function () {
        var lines = [];

        var delta = height / horDimension;

        // skip first and last lines cause that will be border's. Last == horDimensions + 1
        for (var i = 1; i < horDimension; i++) {
            lines.push(new Line(new Point(0, i * delta), new Point(width, i * delta)));
        }

        return lines;
    };

    this.getVerticalLines = function () {
        var lines = [];

        var delta = width / verDimension;

        // skip first and last lines cause that will be border's. Last == horDimensions + 1
        for (var i = 1; i < verDimension; i++) {
            lines.push(new Line(new Point(i * delta, 0), new Point(i * delta, height)));
        }

        return lines;
    };

    this.getPointRectangle = function (p) {

        if (p == undefined || p == null) {
            throw new Error("Invalid point");
        }

        var deltaVer = width / verDimension;
        var deltaHor = height / horDimension;

        // Need to find i, j such as
        // (j - 1) * deltaVer < p.X < j * deltaVer; and
        // (i - 1) * deltaHor < p.Y < i * deltaHor;

        for (var j = 1; j < horDimension; j++) {
            if ((j - 1) * deltaVer < p.X && p.X < j * deltaVer) {
                break;
            }
        }

        for (var i = 0; i < verDimension; i++) {
            if ((i - 1) * deltaHor < p.Y && p.Y < i * deltaHor) {
                break;
            }
        }

        var pLeftTop = new Point((j - 1) * deltaVer, (i - 1) * deltaHor);
        var pRightBottom = new Point(j * deltaVer, i * deltaHor);

        return new Rectangle(pLeftTop, pRightBottom);
    };

    this.getDeltaX = function () {
        return width / verDimension;
    };

    this.getDeltaY = function () {
        return height / horDimension;
    };

    this.drawGrid = function (canvas, context) {
        var canvasHelper = new CanvasHelper(canvas, context);

        var lines = this.getHorizontalLines();

        for (var i = 0; i < lines.length; i++) {
            canvasHelper.drawLine(lines[i]);
        }

        lines = this.getVerticalLines();

        for (var i = 0; i < lines.length; i++) {
            canvasHelper.drawLine(lines[i]);
        }
    };
}