﻿var Shoots = function () {
    var userRectangles = [];
    var opponentRectangles = [];

    function isRectangle(rect) {

        if (rect === undefined || rect === null) {
            throw new Error("null or undefined rectangle");
        }

        if (rect.getLeftTopPoint === undefined || rect.getRightBottomPoint === undefined) {
            throw new Error("object is not rectangle");
        }

        return true;
    };

    this.addUserShoot = function (rect) {

        if (!isRectangle(rect)) {
            return;
        }

        opponentRectangles.push(rect);
    };

    this.addOpponentShoot = function (rect) {

        if (!isRectangle(rect)) {
            return;
        }

        userRectangles.push(rect);
    };

    this.getUserFieldShoots = function () {
        return userRectangles;
    };

    this.getOpponentFieldShoots = function () {
        return opponentRectangles;
    };

};

window.shoots = new Shoots();