﻿var Config = function () {
    const shipRule = [0, 4, 3, 2, 1];

    this.getShipRule = function () {
        return shipRule;
    };

    this.checkFleetToStart = function (fleet) {
        var shipsPerRectangleNum = [0, 0, 0, 0, 0];
        var ships = fleet.getShips();

        for (var i = 0; i < ships.length; i++) {
            shipsPerRectangleNum[ships[i].countRectangles()]++;
        }

        for (var i = 0; i < shipsPerRectangleNum.length; i++) {
            if (shipsPerRectangleNum[i] != shipRule[i]) {
                return false;
            }
        }

        return true;
    };
};

var DynamicSettings = function () {
    
    var receiveUserSteps = false, receiveShipRectangles = true;

    this.isReceiveingUserSteps = function () {
        return receiveUserSteps;
    };

    this.setUserStepsReceiving = function (value) {

        if (value === undefined || value === null) {
            throw new Error("null or undefined value");
        }

        receiveUserSteps = value;
    };

    this.isReceivingShipRectangles = function () {
        return receiveShipRectangles;
    };

    this.setShipRectangleReceiving = function (value) {
        if (value === undefined || value === null) {
            throw new Error("null or undefined value");
        }

        receiveShipRectangles = value;
    };
};

window.Settings = new DynamicSettings();
