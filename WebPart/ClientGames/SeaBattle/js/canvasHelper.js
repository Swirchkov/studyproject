﻿var CanvasHelper = function (canvas, context) {

    //config
    context.strokeStyle = "#c1c1c1";

    function drawRectangle(color, rect) {
        context.fillStyle = color;

        var pLeftTop = rect.getLeftTopPoint();
        var pRightBottom = rect.getRightBottomPoint();

        context.beginPath();

        context.rect(pLeftTop.X, pLeftTop.Y, pRightBottom.X - pLeftTop.X, pRightBottom.Y - pLeftTop.Y);

        context.closePath();

        context.fill();
    };

    function drawCircle(p, radius, color) {
        context.fillStyle = color;

        context.beginPath();
        context.arc(p.X, p.Y, radius, 0, 2 * Math.PI);
        context.closePath();

        context.fill();
    };

    function drawCircleInCenter(rect, color) {
        var pLeftTop = rect.getLeftTopPoint();
        var pRightBottom = rect.getRightBottomPoint();

        var center = new Point((pLeftTop.X + pRightBottom.X) / 2, (pLeftTop.Y + pRightBottom.Y) / 2);

        var deltaX = pRightBottom.X - pLeftTop.X;
        var deltaY = pRightBottom.Y - pLeftTop.Y;

        var min = deltaX < deltaY ? deltaX : deltaY;
        drawCircle(center, min / 6, color);
    };

    this.drawLine = function (line) {

        if (line == undefined || line == null) {
            throw new Error("Invalid line");
        }

        this.drawPointLine(line.Start, line.End);
    };

    this.drawPointLine = function (p1, p2) {

        if (p1 == undefined || p1 == null || p2 == undefined || p2 == null) {
            throw new Error("Invalid points");
        }

        context.beginPath();

        context.moveTo(p1.X, p1.Y);
        context.lineTo(p2.X, p2.Y);

        context.closePath();

        context.stroke();
    };

    this.drawRectangle = function (rect) {
        drawRectangle('#9ed6ff', rect);
    };

    this.drawFleet = function (fleet) {

        if (fleet == undefined || fleet == null) {
            throw new Error("invalid fleet object");
        }

        var ships = fleet.getShips();

        for (var i = 0; i < ships.length; i++) {

            var rectangles = ships[i].getRectangles();

            for (var j = 0; j < rectangles.length; j++) {

                this.drawRectangle(rectangles[j]);

            }
        }
    };

    this.clearCnavas = function () {
        context.clearRect(0, 0, canvas.width, canvas.height);
    };

    this.drawShoots = function (rectangles) {
        if (rectangles === undefined || rectangles === null) {
            throw new Error("null or undefined parameter");
        }

        for (var i = 0; i < rectangles.length; i++) {
            var rectangle = rectangles[i];

            var pLeftTop = rectangle.getLeftTopPoint();
            var pRightBottom = rectangle.getRightBottomPoint();

            var center = new Point((pLeftTop.X + pRightBottom.X) / 2, (pLeftTop.Y + pRightBottom.Y) / 2);

            var deltaX = pRightBottom.X - pLeftTop.X;
            var deltaY = pRightBottom.Y - pLeftTop.Y;

            var min = deltaX < deltaY ? deltaX : deltaY;
            this.drawRedCircle(center, min / 7);
        }
    };

    this.drawRedCircle = function (p, radius) {
        drawCircle(p, radius, 'red');
    };

    this.drawCircle = function (p, radius, color) {
        drawCircle(p, radius, color);
    };

    this.drawHitRectangle = function (rect) {
        context.strokeStyle = "red";

        var pLeftTop = rect.getLeftTopPoint();
        var pRightBottom = rect.getRightBottomPoint();

        context.beginPath();

        context.rect(pLeftTop.X, pLeftTop.Y, pRightBottom.X - pLeftTop.X, pRightBottom.Y - pLeftTop.Y);

        context.closePath();

        context.stroke();
    };

    this.drawRedFilledRectangle = function (rectangle) {
        drawRectangle('red', rectangle);
    };

    this.getPointColor = function (point) {
        var imgdata = context.getImageData(point.X, point.Y, point.X + 1, point.Y + 1);

        return 'rgba(' + imgdata[0] + ", " + imgdata[1] + ", " + imgdata[2] + ", " + imgdata[3] + ")";
    };

    this.saveRect = function (rect) {
        var pLeftTop = rect.getLeftTopPoint();
        var pRightBottom = rect.getRightBottomPoint();

        return context.getImageData(pLeftTop.X, pLeftTop.Y, pRightBottom.X, pRightBottom.Y);
    };

    this.putRect = function (rect, imgData) {
        var pLeftTop = rect.getLeftTopPoint();

        context.putImageData(imgData, pLeftTop.X, pLeftTop.Y);
    };

    this.drawShoot = function (rect) {
        drawCircleInCenter(rect, 'red');
    };

    this.drawMiss = function (rect) {
        drawCircleInCenter(rect, "#9c9c9c");
    };

    this.drawFinalHit = function (rect) {
        var pLeftTop = rect.getLeftTopPoint();
        var pRightTop = rect.getRightTopPoint();

        var pLeftBottom = rect.getLeftBottomPoint();
        var pRightBottom = rect.getRightBottomPoint();

        context.strokeStyle = 'red';

        var deltaX = ( pRightBottom.X - pLeftTop.X ) / 4;
        var deltaY = ( pRightBottom.Y - pLeftTop.Y ) / 4;

        this.drawPointLine( new Point(pLeftTop.X + deltaX, pLeftTop.Y + deltaY) , 
            new Point(pRightBottom.X - deltaX, pRightBottom.Y - deltaY));

        this.drawPointLine(new Point(pRightTop.X - deltaX, pRightTop.Y + deltaY),
            new Point(pLeftBottom.X + deltaX, pLeftBottom.Y - deltaY));
    };
}