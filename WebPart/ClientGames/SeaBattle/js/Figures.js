﻿var Point = function (x, y) {
    this.X = x;
    this.Y = y;

    this.Equals = function (p) {

        if (p === undefined || p === null) {
            return false;
        }

        // duck typing to detect if that object is point
        if (p.X === undefined || p.Y === undefined) {
            return false;
        }

        return this.X == p.X && this.Y == p.Y;
    };
};

var Line = function (pStart, pEnd) {

    if (pStart == undefined || pStart == null || pEnd == undefined || pEnd == null) {
        throw new Error("Invalid points for line");
    }

    this.Start = pStart;
    this.End = pEnd;

};

var Rectangle = function (pLeftUp, pRightDown) {

    this.getLeftTopPoint = function () {
        return pLeftUp;
    };

    this.getLeftBottomPoint = function () {
        return new Point(pLeftUp.X, pRightDown.Y);
    };

    this.getRightTopPoint = function () {
        return new Point(pRightDown.X, pLeftUp.Y);
    };

    this.getRightBottomPoint = function () {
        return pRightDown;
    };

    this.Equals = function (rect) {

        if (rect === undefined || rect === null) {
            return false;
        }

        // duck typing to detect if ovject is rectangle;
        if (rect.getLeftTopPoint === undefined || rect.getRightBottomPoint === undefined) {
            return false;
        }

        var otherLeftTop = rect.getLeftTopPoint(),
            otherRightBottom = rect.getRightBottomPoint();

        return pLeftUp.Equals(otherLeftTop) && pRightDown.Equals(otherRightBottom);
    };

};

var Ship = function () {
    var rectangles = [];

    this.rectangles = rectangles;

    function IsRectangle(rect) {

        if (rect == undefined || rect == null) {
            throw new Error("null or undefined object");
        }

        if (rect.getLeftTopPoint == undefined || rect.getRightBottomPoint == undefined) {
            throw new Error("object is not rectangle");
        }

        return true;

    };

    function IsNeighbours(rect1, rect2) {

        if (!IsRectangle(rect1) || !IsRectangle(rect2)) {
            return;
        }

        if (IsTopFor(rect1, rect2) || IsTopFor(rect2, rect1) || IsLeftFor(rect1, rect2) || IsLeftFor(rect2, rect1)) {
            return true;
        }

        return false;
    };

    function IsTopFor(rectTop, rectBottom) {
        return rectTop.getLeftBottomPoint().Equals(rectBottom.getLeftTopPoint());
    };

    function IsLeftFor(rectLeft, rectRight) {
        return rectLeft.getRightTopPoint().Equals(rectRight.getLeftTopPoint());
    };

    this.getRectangles = function () {
        return rectangles;
    };

    this.addRectangle = function (rect) {

        if (this.canAddRectangle(rect)) {
            rectangles.push(rect);
        }

    };

    this.removeRectangle = function (rect) {

        if (!this.canRemoveRectangle(rect)) {
            return;
        }

        // after verification we are sure that it's first or last rectangle
        if (rectangles[0].Equals(rect)) {
            rectangles.shift();
        }
        else {
            rectangles.pop();
        }
    };

    this.canRemoveRectangle = function (rect) {

        if (!IsRectangle(rect)) {
            return;
        }

        rectangles = rectangles.sort(function (rect1, rect2) {

            var pLeftTop1 = rect1.getLeftTopPoint();
            var pLeftTop2 = rect2.getLeftTopPoint();

            // X or Y must be equal cause it must be straight line
            if (pLeftTop1.X == pLeftTop2.X) {
                return pLeftTop1.Y - pLeftTop2.Y;
            }
            else {
                return pLeftTop1.X - pLeftTop2.X;
            }
        });

        return rectangles[0].Equals(rect) || rectangles[rectangles.length - 1].Equals(rect);
    };

    this.countRectangles = function () {
        return rectangles.length;
    };

    this.canAddRectangle = function (rect) {

        if (!IsRectangle(rect)) {
            return false;
        }

        if (rectangles.length == 0) {
            return true;
        }

        // ship must stay at horizontal or vertical straight line
        var lineByX = true,
            lineByY = true,
            countNeighbours = 0;

        var rectLeftTop = rect.getLeftTopPoint();

        for (var i = 0; i < rectangles.length; i++) {

            if (rectangles[i].Equals(rect)) {
                throw new Error('Rectangle already in the ship');
            }

            if (IsNeighbours(rectangles[i], rect)) {
                countNeighbours++;
            }

            var pLeftTopPoint = rectangles[i].getLeftTopPoint();

            if (lineByX && rectLeftTop.X != pLeftTopPoint.X) {
                // it's no more straight line by x
                lineByX = false;
            }

            if (lineByY && rectLeftTop.Y != pLeftTopPoint.Y) {
                // it's no more straight line by y
                lineByY = false;
            }
        }

        if (!lineByX && !lineByY) {
            // it's not error. Rectangle object is correct. we just can't add rectangle to the specific ship
            return false;
        }

        if (countNeighbours != 1) {
            return false;
        }

        return true;
    };

    this.isNeighbourhoodRectangle = function (rect) {

        for (var i = 0; i < rectangles.length; i++) {

            if (IsNeighbours(rectangles[i], rect)) {
                return true;
            }

        }

        return false;
    }

    this.hasRectangle = function (rect) {

        if (!IsRectangle(rect)) {
            return false;
        }

        for (var i = 0; i < rectangles.length; i++) {

            if (rectangles[i].Equals(rect)) {
                return true;
            }

        }
        return false;
    };

    this.toString = function () {
        return "Корабель имеет " + rectangles.length + " парусов.";
    };
};

var Fleet = function () {
    var ships = [];

    this.getShips = function () {
        return ships;
    };

    function IsShip(ship) {

        if (ship == undefined || ship == null) {
            throw new Error("ship object null or undefined");
        }

        if (ship.addRectangle == undefined || ship.removeRectangle == undefined || ship.countRectangles == undefined) {
            throw new Error("invalid ship object");
        }

        return true;
    };

    function createShip(rect) {

        if (rect == undefined || rect == null) {
            throw new Error("null or undefined rect object");
        }

        if (rect.getLeftTopPoint == undefined || rect.getRightBottomPoint == undefined) {
            throw new Error("argument object is not rectangle");
        }

        var ship = new Ship();

        ship.addRectangle(rect);

        return ship;
    };

    this.addShip = function (ship) {

        if (!IsShip(ship)) {
            return false;
        }

        ships.push(ship);

    };

    this.clearZeroShips = function () {
        var filteredShips = [];

        for (var i = 0; i < ships.length; i++) {

            if (ships[i].countRectangles() > 0) {
                filteredShips.push(ships[i]);
            }

        }

        ships = filteredShips;
    };

    this.addRectangle = function (rect) {
        var count = 0, ship = null, countNeighbours = 0;;

        for (var i = 0; i < ships.length; i++) {

            if (ships[i].hasRectangle(rect)) {

                if (ships[i].canRemoveRectangle(rect)) {

                    ships[i].removeRectangle(rect);
                    this.clearZeroShips();

                }

                return true;

            }

            if (ships[i].canAddRectangle(rect)) {

                ship = ships[i];
                count++;

            }

            if (ships[i].isNeighbourhoodRectangle(rect)) {
                countNeighbours++;
            }

        }

        if (count > 1) {
            return false;
        }

        if (count == 1 && countNeighbours == 1) {
            ship.addRectangle(rect);
            return true;

        }
        else if (countNeighbours == 0) {
            ships.push(createShip(rect));
            return true;
        }

        return false;
    };

    this.toString = function () {

        var shipRectangles = [0, 0, 0, 0, 0];

        for (var i = 0; i < ships.length; i++) {
            shipRectangles[ships[i].countRectangles()]++;
        }

        var view = "Ваши корабли: <br />";

        for (var j = 4; j > 0; j--) {
            view += j + "-парусных кораблей - " + shipRectangles[j] + "<br />";
        }

        return view;
    };

};