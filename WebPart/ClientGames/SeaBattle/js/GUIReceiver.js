﻿var GUIReceiver = function () {

    this.Initialize = function () {
        var self = this;

        $("#userCanvas").on('click', function (event) {

            if (!window.Settings.isReceivingShipRectangles()) {
                return;
            }

            var usergui = window.UserGUI;
            var canvas = document.getElementById("userCanvas");

            usergui.mouseClick(self.getMousePosition(canvas, event));

        });

        $("#opponentCanvas").on('click', function (event) {

            if (!window.Settings.isReceiveingUserSteps()) {
                return;
            }

            var opponentgui = window.opponentGUI;
            var canvas = document.getElementById("opponentCanvas");

            opponentgui.mouseClick(self.getMousePosition(canvas, event));
        });

        $("#startBtn").on('click', function (event) {

            window.Settings.setShipRectangleReceiving(false);

            var server = new Server();
            var argObj = window.UserGUI.prepareServerObject();

            server.RegisterReceiveCallback(serverCallback);
            server.MakeStep(argObj);

            var help = new Help();

            help.displayHelpMessage("makeSteps");

            $("#programView").html("Ожидайте оппонента.");
            $("#startBtn").hide();
        });
    }

    function serverCallback(arg) {

        processLastStepResult(arg);

        var steps = arg.OpponentSteps;

        if (steps === null) {

            $("#programView").html(" Ваш ход. ");

            processGeneralFlags(arg);

            return;
        }
        else {
            var i = 0;

            var usergui = window.UserGUI;
            var rect0 = usergui.getRectangleForStep(steps[0].StepX, steps[0].StepY);

            var counter = 0, color = 'red', radius = countCircleRadiusForRectangle(rect0), saveRect = null, rectangle = null;
            var srcRadius = radius;

            var canvas = document.getElementById("userCanvas");
            var canvasHelper = new CanvasHelper(canvas, canvas.getContext('2d'));

            var timer = setInterval(function () {

                if (saveRect != null && rectangle != null) {
                    canvasHelper.putRect(rectangle, saveRect);
                    saveRect = null;
                    return;
                }

                rectangle = usergui.getRectangleForStep(steps[i].StepX, steps[i].StepY);

                counter++;

                if (radius == srcRadius) { radius *= 2; }
                else { radius = srcRadius }

                var center = getCenterPoint(rectangle);

                saveRect = canvasHelper.saveRect(rectangle);

                canvasHelper.drawCircle(center, radius, color);

                if (counter == 3) {

                    if (saveRect != null && rectangle != null) {
                        canvasHelper.putRect(rectangle, saveRect);
                        saveRect = null;
                    }

                    processStep(steps[i], rectangle);
                    counter = 0;

                    if (i < steps.length - 1) { i++ }
                    else {
                        clearInterval(timer);
                        timer = null;
                        processGeneralFlags(arg);
                    }

                }

            }, 600);

        }

    };

    function processLastStepResult(arg) {
        var opponent = window.opponentGUI;

        if (arg.IsLastStepHit) {
            opponent.drawLastHit();
        }
        else {
            opponent.drawLastMiss();
        }

        if (arg.IsShipHit) {
            opponent.shipHit();
        }
    };

    function processGeneralFlags(arg) {

        if (arg.IsYourStep) {
            window.Settings.setUserStepsReceiving(true);
            $("#programView").html(" Ваш ход. ");
        }
        else {
            window.Settings.setUserStepsReceiving(false);
            $("#programView").html(" Ход оппонента. ");
        }

        if (arg.IsFinished) {
            finishGame(arg.IsWinner);
            return;
        }
    };

    function processStep(step, rectangle) {

        var usergui = window.UserGUI;
        var canvas = document.getElementById("userCanvas");
        var canvasHelper = new CanvasHelper(canvas, canvas.getContext('2d'));

        if (usergui.isShipHit(rectangle)) {
            canvasHelper.drawRedFilledRectangle(rectangle);
        }
        else {
            canvasHelper.drawMiss(rectangle);
        }

    };

    function finishGame(isWinner) {

        window.Settings.setUserStepsReceiving(false);
        window.Settings.setShipRectangleReceiving(false);

        var message = "";

        if (isWinner) { message = "Вы победили в этой игре. Поздравляем. " }
        else { message = "К сожалению Вы в этот раз проиграли. " }

        $("#programView").html(message);

        $("#startBtn").hide();
        $("#returnBtn").show();

    };

    function getCenterPoint(rectangle) {
        
        var pLeftTop = rectangle.getLeftTopPoint();
        var pRightBottom = rectangle.getRightBottomPoint();

        return center = new Point((pLeftTop.X + pRightBottom.X) / 2, (pLeftTop.Y + pRightBottom.Y) / 2);
    };

    function countCircleRadiusForRectangle(rectangle) {

        var pLeftTop = rectangle.getLeftTopPoint();
        var pRightBottom = rectangle.getRightBottomPoint();

        var deltaX = pRightBottom.X - pLeftTop.X;
        var deltaY = pRightBottom.Y - pLeftTop.Y;

        var min = deltaX < deltaY ? deltaX : deltaY;

        return min / 5;
    };

    this.getMousePosition = function (canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            X: (evt.clientX - rect.left) / (rect.right - rect.left) * canvas.width,
            Y: (evt.clientY - rect.top) / (rect.bottom - rect.top) * canvas.height
        };
    }
}