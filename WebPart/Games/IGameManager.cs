﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPart.Games
{
    public interface IGameManager
    {
        /// <summary>
        /// entry point for game managing - start game
        /// </summary>
        /// <param name="user1Id"> first opponent id ( exists always) </param>
        /// <param name="user2Id"> second opponent id. if null first user plays against computer. </param>
        /// <returns> unique game identifier </returns>
        string StartGame(int user1Id, int? user2Id);

        /// <summary>
        /// Exit point for game managing. Call if someone wins or someone disconnect's.
        /// </summary>
        /// <param name="gameId"> unique game identifier </param>
        /// <returns> dictionary where key is userId and value object to send this user. </returns>
        Dictionary<int,object> FinishGame(string gameId);
    }
}
