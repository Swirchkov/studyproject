﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPart.Games
{
    public static class GameIntelectProvider
    {
        private static Dictionary<string, GameIntelect> resolver = new Dictionary<string, GameIntelect>();

        static GameIntelectProvider()
        {
            var games = SeaBattle.StateKeeper.Games;
        }

        public static void RegisterGameIntelect(string Name, IGameManager gameManager, IStepProvider stepProvider) => 
            resolver.Add(Name, new GameIntelect(gameManager, stepProvider));

        internal static GameIntelect ResolveGameIntelect(string Name) => resolver[Name];
    }
}