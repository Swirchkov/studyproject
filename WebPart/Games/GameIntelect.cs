﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPart.Games
{
    internal class GameIntelect
    {
        public GameIntelect(IGameManager gameManager, IStepProvider stepProvider)
        {
            StepProvider = stepProvider;
            GameManager = gameManager;
        }

        public IStepProvider StepProvider { get; set; }
        public IGameManager GameManager { get; set; }
    }
}