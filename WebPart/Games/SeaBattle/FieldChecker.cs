﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPart.Games.SeaBattle.Models;

namespace WebPart.Games.SeaBattle
{
    public static class FieldChecker
    {
        private enum ShipDirection
        {
            None, 
            Top, 
            Left,
            Bottom,
            Right
        }

        public static bool HasHealthRectangles(ItemState[,] field)
        {
            if (field == null) { return true; }

            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    if (field[i, j] == ItemState.Health) { return true; }
                }
            }

            return false;
        }

        public static bool IsShipFinished(ItemState[,] field, int x, int y)
        {
            if (field[y, x] != ItemState.Killed)
            {
                return false;
            }

            return checkItem(field, x, y, ShipDirection.Top) &&
                checkItem(field, x, y, ShipDirection.Bottom) &&
                checkItem(field, x, y, ShipDirection.Left) &&
                checkItem(field, x, y, ShipDirection.Right);
            
        }

        private static bool checkItem(ItemState[,] field, int x, int y, ShipDirection direction)
        {
            if (y < 0 || y >= field.GetLength(0) || x < 0 || x >= field.GetLength(1))
            {
                return true;
            }

            if (field[y, x] == ItemState.Killed)
            {
                Tuple<int, int> position = incrementItemByDirection(x, y, direction);
                return checkItem(field, position.Item1, position.Item2, direction);
            }

            if (field[y, x] == ItemState.Empty_Shoot || field[y, x] == ItemState.Empty)
            {
                return true;
            }

            return false;
        }

        private static Tuple<int, int> incrementItemByDirection(int x, int y, ShipDirection direction)
        {
            if (direction == ShipDirection.Top)
            {
                return new Tuple<int, int>(x, y - 1);
            }

            if (direction == ShipDirection.Bottom)
            {
                return new Tuple<int, int>(x, y + 1);
            }

            if (direction == ShipDirection.Left)
            {
                return new Tuple<int, int>(x - 1, y);
            }
            if (direction == ShipDirection.Right)
            {
                return new Tuple<int, int>(x + 1, y);
            }
            throw new ArgumentException();
        }
    }
}