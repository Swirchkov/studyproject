﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPart.Games.SeaBattle.ClientModels;
using Newtonsoft.Json.Linq;

namespace WebPart.Games.SeaBattle
{
    public class ClientWorker
    {
        public ArgumentObject DecodeUserStep(object argObject)
        {
            JToken mainToken = (JToken)argObject;
            ArgumentObject arg = new ArgumentObject();

            arg.StepX = (int)mainToken.SelectToken("StepX");
            arg.StepY = (int)mainToken.SelectToken("StepY");

            arg.Field = this.convertToIntArray((JArray)mainToken.SelectToken("Field"));
            return arg;
        }

        public string EncodeUserStep(ArgumentObject argObject) => JsonConvert.SerializeObject(argObject);

        private int[][] convertToIntArray(JArray jsArr)
        {
            int[][] arr = new int[jsArr.Count][];
            int i = 0;

            foreach (JToken token in jsArr)
            {
                JArray subArray = (JArray)token;
                arr[i] = new int[subArray.Count];

                int j = 0;

                foreach (JToken subToken in subArray)
                {
                    arr[i][j++] = (int)subToken;

                }
                i++;
            }

            return arr;
        }
    }
}