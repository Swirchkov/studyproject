﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPart.Games.SeaBattle.ClientModels;
using WebPart.Games.SeaBattle.Models;

namespace WebPart.Games.SeaBattle
{
    public static class UserStepper
    {
        public static Dictionary<int, object> NextUserStep(ref Game game, int stepperId, ArgumentObject arg) 
        {
            ItemState[,] userField = null;
            ItemState[,] opponentField = null;

            if (game.User1Id == stepperId)
            {
                if (game.Field1 == null)
                {
                    // it's initial step for this user
                    game.Field1 = getField(arg);

                    if (game.Field2 != null)
                    {
                        return generateInitialSendPackage(game);
                    }

                    return null;
                }

                userField = game.Field1;
                opponentField = game.Field2;
            }
            else
            {
                if (game.Field2 == null)
                {
                    game.Field2 = getField(arg);

                    if (game.Field1 != null)
                    {
                        return generateInitialSendPackage(game);
                    }

                    return null;
                }

                userField = game.Field2;
                opponentField = game.Field1;
            }

            // now it's not initial step so we have to mark step on field and send step invitation to another user

            // mark step on field
            ItemState current = opponentField[arg.StepY, arg.StepX];
            bool isHit = false;
            bool isShipFinished = false;
            Step[] steps = new[] { new Step() { StepX = arg.StepX, StepY = arg.StepY } };

            if (current == ItemState.Health)
            {
                opponentField[arg.StepY, arg.StepX] = ItemState.Killed;
                isHit = true;

                isShipFinished = FieldChecker.IsShipFinished(opponentField, arg.StepX, arg.StepY);

                if (game.User1Id == stepperId)
                {
                    return new Dictionary<int, object>()
                    {
                        { game.User1Id, new ServerObject() { IsYourStep = true, OpponentSteps = null,
                            IsLastStepHit = true, IsShipHit = isShipFinished } },
                        { game.User2Id.Value, new ServerObject() { IsYourStep = false, OpponentSteps = steps } }
                    };
                }
                else
                {
                    return new Dictionary<int, object>()
                    {
                        { game.User1Id, new ServerObject() { IsYourStep = false, OpponentSteps = steps }  },
                        { game.User2Id.Value,  new ServerObject() { IsYourStep = true, OpponentSteps = null, IsLastStepHit = true, IsShipHit = isShipFinished }}
                    };
                }
            }
            else if (current == ItemState.Empty)
            {
                opponentField[arg.StepY, arg.StepX] = ItemState.Empty_Shoot;
            }

            // send invitation 
            if (game.User1Id == stepperId)
            {
                return new Dictionary<int, object>()
                {
                    { game.User1Id, new ServerObject() { IsYourStep = false, IsLastStepHit = isHit,
                        IsShipHit = isShipFinished } },
                    { game.User2Id.Value, new ServerObject() { IsYourStep = true, OpponentSteps = steps }  }
                };
            }
            else
            {
                return new Dictionary<int, object>()
                {
                    { game.User1Id, new ServerObject() { IsYourStep = true, OpponentSteps = steps } },
                    { game.User2Id.Value, new ServerObject() { IsYourStep = false, IsLastStepHit = isHit,
                        IsShipHit = isShipFinished } }
                };
            }
        }

        private static ItemState[,] getField(ArgumentObject arg) => Converter.ToItemStateField(arg.Field);

        private static Dictionary<int, object> generateInitialSendPackage(Game game)
        {
            Random r = new Random();

            int choise = r.Next(2);

            return new Dictionary<int, object>()
            {
                { game.User1Id, new ServerObject() { IsYourStep = choise == 0, OpponentSteps = null} },
                { game.User2Id.Value, new ServerObject() { IsYourStep = choise == 1, OpponentSteps = null }  }
            };
        }
    }
}