﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPart.Games.SeaBattle.ClientModels;
using WebPart.Games.SeaBattle.Models;

namespace WebPart.Games.SeaBattle
{
    public class StepProvider : IStepProvider
    {
        public bool IsFinished(string gameId)
        {
            Game game = StateKeeper.Games.First(g => g.GameId == gameId);

            return !FieldChecker.HasHealthRectangles(game.Field1) || !FieldChecker.HasHealthRectangles(game.Field2);
        }

        public Dictionary<int, object> NextStep(int userId, string gameId, object argObject)
        {
            Game game = StateKeeper.Games.First(g => g.GameId == gameId);

            ClientWorker worker = new ClientWorker();
            ArgumentObject arg = worker.DecodeUserStep(argObject);

            if ((game.User1Id == userId && game.User2Id.HasValue) || ( game.User2Id == userId))
            {
                Dictionary<int, object> retArg = makeUserStep(ref game, userId, arg);
                return retArg; 
            }
            else if (game.User1Id == userId && !game.User2Id.HasValue)
            {
                Dictionary<int, object> retArg = makeComputerStep(game, userId, arg);
                return retArg;
            }

            throw new ArgumentException("Invalid game or user id");
        }

        private Dictionary<int, object> makeUserStep(ref Game game, int stepperId, ArgumentObject arg)
        {
            return UserStepper.NextUserStep(ref game, stepperId, arg);
        }

        private Dictionary<int, object> makeComputerStep(Game game, int stepperId, ArgumentObject arg)
        {
            return ComputerStepper.NextUserStep(game, stepperId, arg);
        }

    }
}