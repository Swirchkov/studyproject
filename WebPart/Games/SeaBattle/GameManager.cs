﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPart.Games.SeaBattle.ClientModels;
using WebPart.Games.SeaBattle.Models;

namespace WebPart.Games.SeaBattle
{
    public class GameManager : IGameManager
    {
        public Dictionary<int,object> FinishGame(string gameId)
        {
            Game item = StateKeeper.Games.First(g => g.GameId == gameId);

            StateKeeper.Games.Remove(item);

            int? winnerId;

            if (FieldChecker.HasHealthRectangles(item.Field1))
            {
                winnerId = item.User1Id;
            }
            else
            {
                winnerId = item.User2Id;
            }

            if (!item.User2Id.HasValue)
            {
                return new Dictionary<int, object>()
                {
                    { item.User1Id, new ServerObject() { IsFinished = true, IsWinner = winnerId == item.User1Id } }
                };
            }
            else
            {
                return new Dictionary<int, object>()
                {
                    { item.User1Id, new ServerObject() { IsFinished = true, IsWinner = winnerId == item.User1Id } },
                    { item.User2Id.Value, new ServerObject() { IsFinished = true,
                        IsWinner = winnerId == item.User2Id } }
                };
            }
        }

        public string StartGame(int user1Id, int? user2Id)
        {
            Game game = new Models.Game()
            {
                User1Id = user1Id,
                User2Id = user2Id,
                GameId = Guid.NewGuid().ToString().Substring(0, 16).Replace('-', 'A')
            };

            StateKeeper.Games.Add(game);

            return game.GameId;
        }
    }
}