﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using WebPart.Games.SeaBattle.ClientModels;
using WebPart.Games.SeaBattle.Models;

namespace WebPart.Games.SeaBattle
{
    public class ComputerStepper
    {
        private enum ShipDirection
        {
            Top,
            Left,
            Bottom,
            Right
        }

        private static Game current = null;

        public static Dictionary<int, object> NextUserStep(Game game, int stepperId, ArgumentObject arg)
        {
            current = game;
            Random r = new Random();
            ServerObject response = new ServerObject() { IsYourStep = true };

            if (game.Field1 == null)
            {
                game.Field1 = Converter.ToItemStateField(arg.Field);
                game.Field2 = generateComputerField();

                int choice = r.Next(2);

                if (choice == 0)
                {
                    return new Dictionary<int, object>()
                    {
                        { stepperId, new ServerObject() { IsYourStep = true, OpponentSteps = null } }
                    };
                }
            }
            else
            {
                ItemState item = game.Field2[arg.StepY, arg.StepX];

                if (item == ItemState.Empty)
                {
                    game.Field2[arg.StepY, arg.StepX] = ItemState.Empty_Shoot;
                    response.IsLastStepHit = false;
                }
                else if (item == ItemState.Health)
                {
                    game.Field2[arg.StepY, arg.StepX] = ItemState.Killed;

                    response.IsLastStepHit = true;
                    response.IsYourStep = true;
                    response.OpponentSteps = null;
                    response.IsShipHit = FieldChecker.IsShipFinished(game.Field2, arg.StepX, arg.StepY);

                    return new Dictionary<int, object>() { { stepperId, response } };
                }
            }

            List<Step> steps = new List<Step>();
            int x, y;

            do
            {
                // now making our step

                x = r.Next(Game.FieldWidth);
                y = r.Next(Game.FieldWidth);

                while (!checkPossibleStep(game.Field1, x, y))
                {
                    x = r.Next(Game.FieldWidth);
                    y = r.Next(Game.FieldWidth);
                }

                markStep(game.Field1, x, y);

                steps.Add(new Step() { StepX = x, StepY = y });
            }
            while (isHit(game.Field1, x, y));

            response.OpponentSteps = steps.ToArray();

            return new Dictionary<int, object>() { { stepperId, response } };
        }

        private static ItemState[,] generateComputerField()
        {
            //                                      Y                   X
            ItemState[,] field = new ItemState[Game.FieldHeight, Game.FieldWidth];

            // index - rectangle number, value - ships number.
            int[] ships = new int[] { 0, 4, 3, 2, 1 };

            for (int i = ships.Length - 1; i > 0; i--)
            {
                for (int j = 0; j < ships[i]; j++)
                {
                    generateShip(field, i);
                }
            }

            return field;
        }

        
        private static void generateShip(ItemState[,] field, int rectNum)
        {
            Random r = new Random();

            // T1 - X coordinate, T2 - Y coordinate
            Tuple<int, int> coordinates = new Tuple<int, int>(r.Next(Game.FieldWidth), r.Next(Game.FieldHeight));
            ShipDirection direction = (ShipDirection)r.Next(Enum.GetNames(typeof(ShipDirection)).Length);

            while (!checkShip(coordinates, field, direction, rectNum))
            {
                coordinates = new Tuple<int, int>(r.Next(Game.FieldWidth), r.Next(Game.FieldHeight));
                direction = (ShipDirection)r.Next(Enum.GetNames(typeof(ShipDirection)).Length);
            }

            placeShip(coordinates, direction, field, rectNum);
        }

        private static void placeShip(Tuple<int, int> coordinates, ShipDirection direction, ItemState[,] field, int rectNum)
        {
            int x = coordinates.Item1;
            int y = coordinates.Item2;

            for (int i = 0; i < rectNum; i++)
            {
                if (direction == ShipDirection.Top)
                {
                    field[y - i, x] = ItemState.Health;
                }

                if (direction == ShipDirection.Bottom)
                {
                    field[y + i, x] = ItemState.Health;
                }

                if (direction == ShipDirection.Left)
                {
                    field[y, x - i] = ItemState.Health;
                }

                if (direction == ShipDirection.Right)
                {
                    field[y, x + i] = ItemState.Health;
                }
            }
        }

        private static bool checkShip(Tuple<int, int> coordinates, ItemState[,] field, ShipDirection direction, int rectNum)
        {
            int x = coordinates.Item1;
            int y = coordinates.Item2;

            //string fieldView = ComputerStepper.fieldView(field);

            for (int i = 0; i < rectNum; i++)
            {
                if (direction == ShipDirection.Top)
                {
                    if (!checkItem(field, x, y - i))
                    {
                        return false;
                    }
                }

                if (direction == ShipDirection.Bottom)
                {
                    if (!checkItem(field, x, y + i))
                    {
                        return false;
                    }
                }

                if (direction == ShipDirection.Left)
                {
                    if (!checkItem(field, x - i, y))
                    {
                        return false;
                    }
                }

                if (direction == ShipDirection.Right)
                {
                    if (!checkItem(field, x + i, y))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static bool checkItem(ItemState[,] field, int x, int y)
        {
            if (x < 0 || y < 0 || y >= field.GetLength(0) || x >= field.GetLength(1))
            {
                return false;
            }

            if (field[y, x] != ItemState.Empty)
            {
                return false;
            }

            int countNeighbours = 0;

            if (y + 1 < field.GetLength(0) && x < field.GetLength(1) && 
                field[y + 1, x] != ItemState.Empty) { countNeighbours++; }

            if ( y - 1 < field.GetLength(0) && y - 1 >= 0 && x < field.GetLength(1) && 
                field[y - 1, x] != ItemState.Empty) { countNeighbours++; }

            if ( y < field.GetLength(0) && x - 1 < field.GetLength(1) && x - 1 >= 0 &&
                field[y, x - 1] != ItemState.Empty) { countNeighbours++; }

            if ( y < field.GetLength(0) && x + 1 < field.GetLength(1) &&  
                field[y, x + 1] != ItemState.Empty) { countNeighbours++; }

            return countNeighbours == 0;
        }

        private static bool checkPossibleStep(ItemState[,] field, int x, int y) 
        {
            return field[y, x] == ItemState.Empty || field[y, x] == ItemState.Health;
        }

        private static void markStep(ItemState[,] field, int x, int y)
        {
            ItemState current = field[y, x];

            if (current == ItemState.Empty)
            {
                field[y, x] = ItemState.Empty_Shoot;
            }

            if (current == ItemState.Health)
            {
                field[y, x] = ItemState.Killed;
            }
        }

        private static bool isHit(ItemState[,] field, int x, int y)
        {
            return field[y, x] == ItemState.Killed;
        }
    }
}