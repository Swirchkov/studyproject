﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPart.Games.SeaBattle.Models;

namespace WebPart.Games.SeaBattle
{
    public static class Converter
    {
        public static ItemState[,] ToItemStateField(int[][] field)
        {
            ItemState[,] result = new ItemState[field.Length, field[0].Length];

            for (var i = 0; i < field.Length; i++)
            {
                for (var j = 0; j < field[i].Length; j++)
                {
                    result[i, j] = (ItemState)field[i][j];
                }
            }

            return result;
        }

        public static int[][] FromItemStateField(ItemState[,] field)
        {
            int[][] result = new int[field.Length][];

            for (var i = 0; i < field.Length; i++)
            {
                result[i] = new int[field.GetLength(i)];

                for (int j = 0; j < field.GetLength(i); j++)
                {
                    result[i][j] = (int)field[i, j];
                }
            }

            return result;
        }
    }
}