﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebPart.Games.SeaBattle.Models
{
    public class Game
    {
        public string GameId { get; set; }
        public int User1Id { get; set; }
        public int? User2Id { get; set; }
        public ItemState[,] Field1 { get; set; }
        public ItemState[,] Field2 { get; set; }

        public static int FieldWidth { get; } = 8;
        public static int FieldHeight { get; } = 8;

        public string Field1View
        {
            get
            { 
                return fieldView(this.Field1);
            }
        }

        public string Field2View
        {
            get
            {
                return fieldView(this.Field2);
            }
        }

        private string fieldView(ItemState[,] field)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("  |");
            for (int i = 0; i < field.GetLength(1); i++)
            {
                sb.Append(" " + i + " |");
            }

            sb.AppendLine();
            sb.AppendLine(new string('-', 3 + 4 * field.GetLength(1)));

            for (int y = 0; y < field.GetLength(0); y++)
            {
                sb.Append(" " + y + "|");

                for (int x = 0; x < field.GetLength(1); x++)
                {
                    sb.Append(' ');
                    ItemState item = field[y, x];

                    if (item == ItemState.Empty) { sb.Append(" "); }
                    else if (item == ItemState.Empty_Shoot) { sb.Append("S"); }
                    else if (item == ItemState.Health) { sb.Append("H"); }
                    else if (item == ItemState.Killed) { sb.Append("K"); }

                    sb.Append(" |");
                }

                sb.AppendLine();

                sb.AppendLine(new String('-', 3 + 4 * field.GetLength(1)));
            }

            return sb.ToString();
        }
    }
}