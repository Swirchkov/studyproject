﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPart.Games.SeaBattle
{
    public static class StateKeeper
    {
        private static readonly string gameName = "SeaBattle"; 
        static StateKeeper()
        {
            GameIntelectProvider.RegisterGameIntelect(gameName, new GameManager(), new StepProvider());
        }
        public static ICollection<Models.Game> Games { get; } = new List<Models.Game>();
    }
}