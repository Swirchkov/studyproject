﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPart.Games.SeaBattle.ClientModels
{
    public class ArgumentObject
    {
        public int StepX { get; set; }
        public int StepY { get; set; }
        public int[][] Field { get; set; }
    }
}