﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPart.Games.SeaBattle.ClientModels
{
    public class ServerObject
    {

        public bool IsYourStep { get; set; }
        public Step[] OpponentSteps { get; set; }
        public bool IsFinished { get; set; }
        public bool IsWinner { get; set; }
        public bool IsLastStepHit { get; set; }
        public bool IsShipHit { get; set; }
    }

    public class Step
    {
        public int StepX { get; set; }
        public int StepY { get; set; }
    }
}