﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPart.Games
{
    public interface IStepProvider
    {
        /// <summary>
        /// Receives next step from client.
        /// </summary>
        /// <param name="userId"> Client Id. </param>
        /// <param name="gameId"> Game Id. </param>
        /// <param name="argObject"> Argument received from client. </param>
        /// <returns> dictionary where key is userId, and value - object to send this user. </returns>
        Dictionary<int,object> NextStep(int userId, string gameId, object argObject);

        /// <summary>
        /// Detects if game has finished.
        /// </summary>
        /// <param name="gameId"> Unique game identifier. </param>
        /// <returns> True - if the game is finished, other -false. </returns>
        bool IsFinished(string gameId);
    }
}
