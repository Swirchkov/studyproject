﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Threading;

namespace WebPart.Util
{
    internal static class FileWorker
    {
        private static object lockObject = new object();

        // returns filename of saved file. Possibly name from HttpPosted object or randomly generated
        internal static string SaveFile(HttpPostedFileBase file, string directory)
        {
            string fileName = file.FileName;
            string extension = fileName.Split('.').Last();
            string path = Path.Combine(directory, fileName);

            lock (lockObject)                       // to exclude that minimum chance for name conflict
            {                                       // that can happen with multiple threads tring to save file
                while (File.Exists(path))
                {
                    fileName = Guid.NewGuid().ToString().Replace('-', 'A').Substring(0, 10) + '.' + extension;
                    path = Path.Combine(directory, fileName);
                }

                file.SaveAs(path);
            }

            return fileName;
        }
    }
}