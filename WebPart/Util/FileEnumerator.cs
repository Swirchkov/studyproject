﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebPart.Util
{
    internal static class FileEnumerator
    {
        public static IEnumerable<string> EnumerateFiles(string directoryPath, Predicate<string> predicate = null, string pattern = "*")
        {
            DirectoryInfo directory = new DirectoryInfo(directoryPath);

            foreach (var file in directory.EnumerateFiles(pattern, SearchOption.AllDirectories))
            {
                if (predicate == null || predicate(file.Name))
                {
                    yield return getRelativeToDirectotyPath(directory, file);
                }
            }
        }

        private static string getRelativeToDirectotyPath(DirectoryInfo directory, FileInfo file)
        {
            string relativePath = "/" + file.Name;
            DirectoryInfo parentDir = file.Directory;

            while (!parentDir.FullName.Equals(directory.FullName, StringComparison.OrdinalIgnoreCase))
            {
                relativePath = "/" + parentDir.Name + relativePath;
                parentDir = parentDir.Parent;
            }

            return relativePath;
        }
    }
}