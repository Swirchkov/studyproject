﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BllPart.Services;
using BllPart.DTO;

namespace WebPart.Util
{
    internal static class GameInitializer
    {
        internal static void Initialize()
        {
            GameInfoDTO seaBattle = new GameInfoDTO()
            {
                Name = "Морской бой",
                Description = @"Старая добрая всем знакомая с детства игра. Мы же предлагаем свой вариант переноса игры 
                                на компьютерную платформу. Попробуйте и Вы не пожалеете о своей попытке.",
                Avatar = "/Files/Games/sea-battle.jpg"
            };

            IGameService findService = DependencyResolver.Current.GetService<IGameService>();

            if (findService.FindInfos(g => g.Name == seaBattle.Name).Count() == 0)
            {
                createGameInfo(seaBattle);
            }
        }

        private static void createGameInfo(GameInfoDTO item)
        {
            IModificationService<GameInfoDTO> modifyService = DependencyResolver.Current.GetService<IModificationService<GameInfoDTO>>();

            modifyService.CreateItem(item);
        } 
    }
}