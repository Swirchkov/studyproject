﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using BllPart.DI;
using Autofac;
using Autofac.Integration.Mvc;

namespace WebPart.DI
{
    internal static class AutofacContainer
    {
        internal static void ConfigContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule(new BLLModule());

            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}