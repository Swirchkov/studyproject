﻿$(function () {
    $("#waitAnimation").hide();
    $("#showOpponent").hide();
});

var hub = $.connection.searchHub;

hub.client.log = function (message) {
    console.log(message);
}

hub.client.setOpponent = function (user, redirectUrl) {

    console.log(" Set opp");
    console.log(user);
    console.log(redirectUrl);

    user = JSON.parse(user);

    $("#waitAnimation").hide();
    $("#showOpponent").show();

    $("#opponentName").text(user.DisplayName);
    $("#Opponent-Img").attr("src", user.Avatar);

    var seconds = 5;
    $(".user-container").text(" Игра начнется через " + seconds);

    var timer = setInterval(function () {
        seconds--;

        $(".user-container").text(" Игра начнется через " + seconds);

        if (seconds == 0) {
            clearInterval(timer);
            location.href = redirectUrl;
        }
    }, 1000);


};

$.connection.hub.start().done(function () {

    console.log("hub from client");
    console.log(hub);

    $("#realGame").click(function (event) {
        event.preventDefault();

        $("#waitAnimation").show();

        hub.server.registerUser(window.UserId, window.GameInfoId);
    });

    $("#computerGame").click(function (event) {
        event.preventDefault();

        $("#waitAnimation").show();

        hub.server.registerGameWithComputer(window.UserId, window.GameInfoId);
    });
});