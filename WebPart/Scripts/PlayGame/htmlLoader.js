﻿var htmlLoader = function () {
    var obj = {};

    obj.loadGameHtml = function (path) {
        $.get(path, function (data) {
            $("#gameContent").html(data);
            window.ongamehtmlloaded();
        });

    };

    return obj;
};
