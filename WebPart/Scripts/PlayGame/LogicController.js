﻿var Server = function () {

    this.MakeStep = function (argObj) {
        comuunicator.sendUserStep(argObj);
    };

    this.RegisterReceiveCallback = function (callback) {

        if (window.functions === undefined) {
            window.functions = [];
        }

        window.functions.push(callback);
    };

    this.ServerCallback = function (argObj) {

        console.log(argObj);

        var functions = window.functions

        if (functions === undefined) {
            return;
        }

        for (var i = 0; i < functions.length; i++) {
            functions[i].apply(null, [ argObj ]);
        }

    };

    var comuunicator = new ServerCommunicator(this.ServerCallback);
}