﻿var hub = $.connection.gameHub;

var callback = null;

hub.client.clientCallback = function (serverObject) {
    callback(serverObject);
};

$.connection.hub.start().done(function () {

    hub.server.saveId(UserData.UserId);

    window.ServerCommunicator = function (receiveCallback) {

        if (receiveCallback === undefined || receiveCallback === null || typeof (receiveCallback) != 'function') {
            console.error(receiveCallback);
            console.log(typeof (receiveCallback));
            throw new Error('Invalid callback');
        }

        callback = receiveCallback;

        this.sendUserStep = function (argObj) {
            hub.server.receiveUserStep(UserData.GameId, UserData.UserId, argObj);
        }

    };
});