﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using BllPart.DTO;
using BllPart.Services;
using Newtonsoft.Json;

namespace WebPart.Hubs
{
    public class SearchHub : Hub
    {
        private class UserRequest
        {
            public int UserId { get; set; }
            public string ConnectionId { get; set; }
            public int GameInfoId { get; set; }
        }

        private static List<UserRequest> requests = new List<UserRequest>();

        public void RegisterUser(int userId, int gameInfoId)
        {
            Clients.All.log("Hello from server");

            if (!existsGameInfoId(gameInfoId) || !existsUserId(userId))
            {
                return;
            }

            UserRequest request = new UserRequest()
            {
                UserId = userId,
                GameInfoId = gameInfoId,
                ConnectionId = Context.ConnectionId
            };

            lock (requests)
            {
                UserRequest opponent = requests.Find(req => req.GameInfoId == gameInfoId);
                if (opponent == null)
                {
                    requests.Add(request);
                }
                else
                {
                    requests.Remove(opponent);
                    GameDTO gameItem = createGame(request, opponent);
                    sendNotifications(request, opponent, gameItem);

                }
            }
        }

        public void RegisterGameWithComputer(int userId, int gameInfoId)
        {
            if (!existsGameInfoId(gameInfoId) || !existsUserId(userId))
            {
                return;
            }

            GameDTO item = createGameWithComputer(userId, gameInfoId);

            sendUserNotification(item);
        }

        private bool existsUserId(int userId)
        {
            IUserService service = DependencyResolver.Current.GetService<IUserService>();
            return service.Find(u => u.UserId == userId).Count() > 0;
        }

        private bool existsGameInfoId(int gameInfoId)
        {
            IGameService service = DependencyResolver.Current.GetService<IGameService>();
            return service.FindInfos(info => info.Id == gameInfoId).Count() > 0;
        }

        private void sendNotifications(UserRequest user1, UserRequest user2, GameDTO item)
        {
            IUserService service = DependencyResolver.Current.GetService<IUserService>();

            UserDTO dto1 = service.Find(u => u.UserId == user1.UserId).FirstOrDefault();
            UserDTO dto2 = service.Find(u => u.UserId == user2.UserId).FirstOrDefault();

            // stringify dto object

            string serialized1 = JsonConvert.SerializeObject(new { Avatar = dto1.Avatar, DisplayName = dto1.DisplayName });
            string serialized2 = JsonConvert.SerializeObject(new { Avatar = dto2.Avatar, DisplayName = dto2.DisplayName });

            Clients.Client(user1.ConnectionId).setOpponent( serialized2 , "/Game/StartGame/" + item.GameId);
            Clients.Client(user2.ConnectionId).setOpponent( serialized1 , "/Game/StartGame/" + item.GameId);
        }

        private void sendUserNotification(GameDTO game)
        {
            string userObj = JsonConvert.SerializeObject(new { DisplayName = "Исскуственный интелект", Avatar = "/Files/Games/sea-battle.jpg" });

            Clients.Caller.setOpponent(userObj, "/Game/StartGame/" + game.GameId);
        }

        private GameDTO createGame(UserRequest user1, UserRequest user2)
        {
            GameDTO game = new GameDTO()
            {
                User1Id = user1.UserId,
                User2Id = user2.UserId,
                GameInfoId = user1.GameInfoId
            };

            IModificationService<GameDTO> modifyService = DependencyResolver.Current.GetService<IModificationService<GameDTO>>();

            modifyService.CreateItem(game);                 // validation exception is impossible

            IGameService findService = DependencyResolver.Current.GetService<IGameService>();

            return findService.FindGames(g => g.User1Id == game.User1Id && g.User2Id == game.User2Id).Last();
        }

        private GameDTO createGameWithComputer(int userId, int gameInfoId)
        {
            GameDTO game = new GameDTO()
            {
                User1Id = userId,
                User2Id = null,
                GameInfoId = gameInfoId
            };

            IModificationService<GameDTO> modifyService = DependencyResolver.Current.GetService<IModificationService<GameDTO>>();

            modifyService.CreateItem(game);

            IGameService service = DependencyResolver.Current.GetService<IGameService>();
            return service.FindGames(g => g.User1Id == userId && g.GameInfoId == gameInfoId).Last();
        }
    }
}