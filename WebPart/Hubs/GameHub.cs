﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json.Linq;
using WebPart.Games;
using BllPart.DTO;
using BllPart.Services;
using System.Threading.Tasks;

namespace WebPart.Hubs
{
    public class GameHub : Hub
    {
        // match between user connection id and playing game
        private static Dictionary<string, string> userGames = new Dictionary<string, string>();
        // match between user id and user connection id
        private static Dictionary<int, string> userConnections = new Dictionary<int, string>();

        public void Hello()
        {
            Clients.All.hello();
        }

        public void SaveId(int userId)
        {
            userConnections.Add(userId, Context.ConnectionId);
        }

        public void ReceiveUserStep(int gameId, int userId, object argObj)
        {
            string connectionId = Context.ConnectionId;
            string gameServerId = null;
            bool existskey = userGames.TryGetValue(connectionId, out gameServerId);

            if (!existskey)
            {
                gameServerId = startGame(gameId, userId);
                userGames[connectionId] = gameServerId;
            }

            GameIntelect intelect = resolveGameIntelect(gameId);
            var sendDictionary = intelect.StepProvider.NextStep(userId, gameServerId, argObj);

            if (sendDictionary != null)
            {
                this.callCallbacks(sendDictionary);
            }

            // check if game is finished

            if (intelect.StepProvider.IsFinished(gameServerId))
            {
                sendDictionary = intelect.GameManager.FinishGame(gameServerId);
                this.callCallbacks(sendDictionary);
            }
         }

        public override Task OnDisconnected(bool stopCalled)
        {
            string connectionId = Context.ConnectionId;

            var userId = userConnections.First(pair => pair.Value == connectionId).Key;

            userConnections.Remove(userId);

            return base.OnDisconnected(stopCalled);
        }

        private string startGame(int gameId, int userId)
        {
            IGameService service = DependencyResolver.Current.GetService<IGameService>();

            GameDTO game = service.FindGames(g => g.GameId == gameId).First();
            GameInfoDTO info = game.GameInfo;

            GameIntelect intelect = GameIntelectProvider.ResolveGameIntelect(info.ServerName);

            lock (userGames)
            {

                if (game.User1Id == userId)
                {
                    if (game.User2Id.HasValue)
                    {
                        int user2Id = game.User2Id.Value;

                        string connection2Id = null;

                        if (userConnections.TryGetValue(user2Id, out connection2Id))
                        {
                            string gameServerId = null;

                            if (userGames.TryGetValue(connection2Id, out gameServerId))
                            {
                                return gameServerId;
                            }
                            else
                            {
                                return intelect.GameManager.StartGame(game.User1Id, game.User2Id);
                            }
                        }
                        else
                        {
                            return intelect.GameManager.StartGame(game.User1Id, game.User2Id);
                        }
                    }
                    else
                    {
                        return intelect.GameManager.StartGame(game.User1Id, game.User2Id);
                    }
                }
                else if (game.User2Id.HasValue && game.User2Id.Value == userId)
                {
                    int user1Id = game.User1Id;

                    string connectionId = null;

                    if (userConnections.TryGetValue(user1Id, out connectionId))
                    {
                        string gameServerId = null;

                        if (userGames.TryGetValue(connectionId, out gameServerId))
                        {
                            return gameServerId;
                        }
                        else
                        {
                            return intelect.GameManager.StartGame(game.User1Id, game.User2Id);
                        }
                    }
                    else
                    {
                        return intelect.GameManager.StartGame(game.User1Id, game.User2Id);
                    }
                }
            }

            throw new ArgumentException();
        }

        private GameIntelect resolveGameIntelect(int gameId)
        {
            IGameService service = DependencyResolver.Current.GetService<IGameService>();

            GameDTO game = service.FindGames(g => g.GameId == gameId).First();
            GameInfoDTO info = game.GameInfo;

            return GameIntelectProvider.ResolveGameIntelect(info.ServerName);
        }

        private void callCallbacks(Dictionary<int, object> dictionary)
        {
            foreach (int id in dictionary.Keys)
            {
                string connectionId = userConnections[id];
                Clients.Client(connectionId).clientCallback(dictionary[id]);
            }
        }
    }
}