﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPart.Models.Game
{
    public class StartGameViewModel
    {
        public int GameId { get; set; }
        public int UserId { get; set; }
        public string GameName { get; set; }
        public string OpponentName { get; set; }
        public string ServerGameName { get; set; }
        public IEnumerable<string> CssLinks { get; set; }
        public IEnumerable<string> JsLinks { get; set; }
    }
}