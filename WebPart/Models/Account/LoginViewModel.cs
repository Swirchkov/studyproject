﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPart.Models.Account
{
    public class LoginViewModel
    {
        public string Login { get; set; }
        public string Password { get; set; }

    }
}