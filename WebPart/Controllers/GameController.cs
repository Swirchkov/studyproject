﻿using BllPart.DTO;
using BllPart.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPart.Filters;
using WebPart.Models.Game;
using WebPart.Util;

namespace WebPart.Controllers
{
    public class GameController : Controller
    {
        private StartGameViewModel createStartGameModel(GameDTO game)
        {
            // game
            StartGameViewModel model = new StartGameViewModel();

            model.GameId = game.GameId;
            model.GameName = game.GameInfo.Name;
            model.ServerGameName = game.GameInfo.ServerName;

            // opponent
            UserDTO user = (UserDTO)Session["User"];

            model.UserId = user.UserId;
            model.OpponentName = detectOpponentName(game, user);

            // links
            model.CssLinks = FileEnumerator.EnumerateFiles(Server.MapPath("/ClientGames/" + model.ServerGameName), pattern: "*.css").Select(s => "/ClientGames/" + model.ServerGameName + s);

            model.JsLinks = FileEnumerator.EnumerateFiles(Server.MapPath("/ClientGames/" + model.ServerGameName), pattern: "*.js").Select(s => "/ClientGames/" + model.ServerGameName + s);

            return model;
        }
        private string detectOpponentName(GameDTO game, UserDTO user)
        {
            if (user.UserId == game.User1Id)
            {
                if (game.User2Id.HasValue)
                {
                    return game.User2.DisplayName;
                }
                else
                {
                    return "Компьютер";
                }
            }
            else if (game.User2Id.HasValue && game.User2Id.Value == user.UserId)
            {
                return game.User1.DisplayName;
            }

            throw new ArgumentException("This user doesn't participate in this game");
        }
        
        // GET: Game
        [AuthorizeFilter]
        public ActionResult Index()
        {
            IGameService service = DependencyResolver.Current.GetService<IGameService>();
            IEnumerable<GameInfoDTO> games = service.FindInfos(info => true);
            return View(games);
        }

        [AuthorizeFilter]
        public ActionResult SearchOpponent(int id)
        {
            IGameService gameService = DependencyResolver.Current.GetService<IGameService>();
            GameInfoDTO gameInfo = gameService.FindInfos(info => info.Id == id).FirstOrDefault();

            if (gameInfo == null)
            {
                return HttpNotFound();
            }

            return View(gameInfo);
        }

        public ActionResult StartGame(int id)
        {
            IGameService gameService = DependencyResolver.Current.GetService<IGameService>();

            StartGameViewModel model;

            try
            {
                model = createStartGameModel(gameService.FindGames(g => g.GameId == id).FirstOrDefault());
            }
            catch (ArgumentException)
            {
                return HttpNotFound();
            }

            return View(model);
        }

    }
}