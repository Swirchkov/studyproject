﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPart.Models.Account;
using BllPart.Services;
using BllPart.DTO;
using BllPart.Validation;
using WebPart.Util;

namespace WebPart.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            IUserService service = DependencyResolver.Current.GetService<IUserService>();

            UserDTO user = service.Login(model.Login, model.Password);

            if (user == null)
            {
                ViewBag.Error = "Неверный логин или пароль";
                return View();
            }

            Session["User"] = user;

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            // creating user to place in db
            UserDTO user = new UserDTO()
            {
                DisplayName = model.DisplayName,
                Login = model.Login,
                Password = model.Password,
                Email = model.Email,
                Avatar = "|| standart path ||"
            };

            // trying to save in db
            IUserService service = DependencyResolver.Current.GetService<IUserService>();

            try
            {
                service.Register(user);
                user = service.Find(u => u.Login == user.Login).FirstOrDefault();
            }
            catch (ValidationException ex)
            {
                ViewBag.Error = ex.ExceptionType.ToString();
                return View();
            }

            if (model.Avatar != null)
            {
                user.Avatar = "/Files/Images/" +  FileWorker.SaveFile(model.Avatar, Server.MapPath("~/Files/Images"));
            }

            IModificationService<UserDTO> modifyService = DependencyResolver.Current.GetService<IModificationService<UserDTO>>();

            modifyService.UpdateItem(user);         // validation exception is impossible

            return RedirectToAction("Index", "Home");
        }
    } 
}