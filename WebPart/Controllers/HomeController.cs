﻿using BllPart.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPart.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            UserDTO user = (UserDTO)Session["User"];

            if (user == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Game");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}