﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DalPart.Models;
using DalPart.Repositories;
using Autofac;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            IRepository<GameInfo> repo = Container.Resolver.Resolve<IRepository<GameInfo>>();

            repo.Add(new GameInfo()
            {
                Name = "sea battle"
            });

            Console.WriteLine("Success");
        }
    }
}
