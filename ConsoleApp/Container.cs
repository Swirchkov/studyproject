﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace ConsoleApp
{
    static class Container
    {
        private static IContainer _container;

        static Container()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule<DalPart.DI.DalModule>();

            _container = builder.Build();
        }

        public static IContainer Resolver
        {
            get
            {
                return _container;
            }
        } 
    }
}
