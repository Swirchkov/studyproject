﻿namespace BllPart.Services
{
    public interface IModificationService<T> where T : class
    {
        void CreateItem(T item);
        void UpdateItem(T item);
        void DeleteItem(int id);
    }
}
