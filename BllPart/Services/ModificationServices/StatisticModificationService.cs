﻿using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;
using BllPart.Validation;

namespace BllPart.Services.ModificationServices
{
    internal class StatisticModificationService : IModificationService<StatisticDTO>
    {
        private IRepository<Statistic> repo = Container.Resolver.Resolve<IRepository<Statistic>>();
        private IValidator<StatisticDTO> validator = Container.Resolver.Resolve<IValidator<StatisticDTO>>();

        public StatisticModificationService() { }

        public void CreateItem(StatisticDTO item)
        {
            if (validator.CheckEntityExisting(item.StatisticId))
            {
                throw new ValidationException(ValidationMistake.Entity_Already_Exists);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Add(item.ToDomainObject());
        }

        public void DeleteItem(int id)
        {
            if (!validator.CheckEntityExisting(id))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }
            repo.Delete(id);
        }

        public void UpdateItem(StatisticDTO item)
        {
            if (!validator.CheckEntityExisting(item.StatisticId))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Update(item.ToDomainObject());
        }
    }
}
