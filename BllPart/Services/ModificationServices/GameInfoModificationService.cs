﻿using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;
using BllPart.Validation;

namespace BllPart.Services.ModificationServices
{
    internal class GameInfoModificationService : IModificationService<GameInfoDTO>
    {
        private IRepository<GameInfo> repo = Container.Resolver.Resolve<IRepository<GameInfo>>();
        private IValidator<GameInfoDTO> validator = Container.Resolver.Resolve<IValidator<GameInfoDTO>>();

        public GameInfoModificationService() { }

        public void CreateItem(GameInfoDTO item)
        {
            if (validator.CheckEntityExisting(item.Id))
            {
                throw new ValidationException(ValidationMistake.Entity_Already_Exists);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Add(item.ToDomainObject());
        }

        public void DeleteItem(int id)
        {
            if (!validator.CheckEntityExisting(id))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }
            repo.Delete(id);
        }

        public void UpdateItem(GameInfoDTO item)
        {
            if (!validator.CheckEntityExisting(item.Id))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Update(item.ToDomainObject());
        }
    }
}
