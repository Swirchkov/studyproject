﻿using BllPart.DTO;
using DalPart.Repositories;
using DalPart.Models;
using BllPart.DI;
using Autofac;
using BllPart.Validation;

namespace BllPart.Services.ModificationServices
{
    internal class GameModificationService : IModificationService<GameDTO>
    {
        private IRepository<Game> repo = Container.Resolver.Resolve<IRepository<Game>>();
        private IValidator<GameDTO> validator = Container.Resolver.Resolve<IValidator<GameDTO>>();

        public GameModificationService() { }

        public void CreateItem(GameDTO item)
        {
            if (validator.CheckEntityExisting(item.GameId))
            {
                throw new ValidationException(ValidationMistake.Entity_Already_Exists);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Add((Game)item);
        }

        public void DeleteItem(int id)
        {
            if (!validator.CheckEntityExisting(id))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }

            repo.Delete(id);
        }

        public void UpdateItem(GameDTO item)
        {
            if (!validator.CheckEntityExisting(item.GameId))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Update((Game)item);
        }
    }
}
