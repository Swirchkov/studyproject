﻿using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;
using BllPart.Validation;

namespace BllPart.Services.ModificationServices
{
    internal class UserModificationService : IModificationService<UserDTO>
    {
        private IRepository<User> repo = Container.Resolver.Resolve<IRepository<User>>();
        private IValidator<UserDTO> validator = Container.Resolver.Resolve<IValidator<UserDTO>>();

        public UserModificationService() { }

        public void CreateItem(UserDTO item)
        {
            if (validator.CheckEntityExisting(item.UserId))
            {
                throw new ValidationException(ValidationMistake.Entity_Already_Exists);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Add(item.ToDomainObject());
        }

        public void DeleteItem(int id)
        {
            if (!validator.CheckEntityExisting(id))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }
            repo.Delete(id);
        }

        public void UpdateItem(UserDTO item)
        {
            if (!validator.CheckEntityExisting(item.UserId))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Update(item.ToDomainObject());
        }
    }
}
