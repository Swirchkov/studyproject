﻿using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;
using BllPart.Validation;

namespace BllPart.Services.ModificationServices
{
    internal class FeedbackModificationService : IModificationService<FeedbackDTO>
    {
        private IRepository<Feedback> repo = Container.Resolver.Resolve<IRepository<Feedback>>();
        private IValidator<FeedbackDTO> validator = Container.Resolver.Resolve<IValidator<FeedbackDTO>>();

        public FeedbackModificationService() { }

        public void CreateItem(FeedbackDTO item)
        {
            if (validator.CheckEntityExisting(item.FeedbackId))
            {
                throw new ValidationException(ValidationMistake.Entity_Already_Exists);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Add(item.ToDomainObject());
        }

        public void DeleteItem(int id)
        {
            if (!validator.CheckEntityExisting(id))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }
            repo.Delete(id);
        }

        public void UpdateItem(FeedbackDTO item)
        {
            if (!validator.CheckEntityExisting(item.FeedbackId))
            {
                throw new ValidationException(ValidationMistake.Entity_Not_Found);
            }

            if (!validator.ValidateEntity(item))
            {
                return;
            }

            repo.Update(item.ToDomainObject());
        }
    }
}
