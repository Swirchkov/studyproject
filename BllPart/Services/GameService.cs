﻿using System;
using System.Collections.Generic;
using System.Linq;
using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;

namespace BllPart.Services
{
    internal class GameService : IGameService
    {
        public GameService() { }

        public IEnumerable<GameDTO> FindGames(Func<GameDTO, bool> predicate)
        {
            IRepository<Game> repo = Container.Resolver.Resolve<IRepository<Game>>();

            return repo.Find(g => predicate((GameDTO)g)).Select(g => (GameDTO)g);
        }

        public IEnumerable<GameInfoDTO> FindInfos(Func<GameInfoDTO, bool> predicate)
        {
            IRepository<GameInfo> repo = Container.Resolver.Resolve<IRepository<GameInfo>>();

            return repo.Find(g => predicate((GameInfoDTO)g)).Select(g => (GameInfoDTO)g);
        }
    }
}
