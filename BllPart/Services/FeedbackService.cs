﻿using System;
using System.Collections.Generic;
using System.Linq;
using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;

namespace BllPart.Services
{
    internal class FeedbackService : IFeedbackService
    {
        public FeedbackService() { }

        public IEnumerable<FeedbackDTO> Find(Func<FeedbackDTO, bool> predicate)
        {
            IRepository<Feedback> repo = Container.Resolver.Resolve<IRepository<Feedback>>();

            return repo.Find(g => predicate((FeedbackDTO)g)).Select(g => (FeedbackDTO)g);
        }
    }
}
