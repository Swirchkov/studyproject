﻿using System;
using System.Collections.Generic;
using BllPart.DTO;

namespace BllPart.Services
{
    public interface IGameService
    {
        IEnumerable<GameDTO> FindGames(Func<GameDTO, bool> predicate);
        IEnumerable<GameInfoDTO> FindInfos(Func<GameInfoDTO, bool> predicate);
    }
}
