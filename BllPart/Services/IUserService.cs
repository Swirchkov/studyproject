﻿using System;
using System.Collections.Generic;
using BllPart.DTO;

namespace BllPart.Services
{
    public interface IUserService
    {
        void Register(UserDTO user);
        UserDTO Login(string login, string password);
        StatisticDTO GetUserStatistic(UserDTO user, GameInfoDTO game);
        IEnumerable<UserDTO> Find(Func<UserDTO, bool> predicate);
    }
}
