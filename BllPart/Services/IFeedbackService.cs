﻿using System;
using System.Collections.Generic;
using BllPart.DTO;

namespace BllPart.Services
{
    interface IFeedbackService
    {
        IEnumerable<FeedbackDTO> Find(Func<FeedbackDTO, bool> predicate);
    }
}
