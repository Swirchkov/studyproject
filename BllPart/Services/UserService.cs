﻿using System;
using System.Collections.Generic;
using System.Linq;
using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;

namespace BllPart.Services
{
    internal class UserService : IUserService
    {
        private IRepository<User> userRepo = Container.Resolver.Resolve<IRepository<User>>();

        public UserService() { }

        public IEnumerable<UserDTO> Find(Func<UserDTO, bool> predicate) => userRepo.Find(user => predicate((UserDTO)user)).Select(user => (UserDTO)user);

        public StatisticDTO GetUserStatistic(UserDTO user, GameInfoDTO gameInfo)
        {
            IRepository<Statistic> repo = Container.Resolver.Resolve<IRepository<Statistic>>();

            return (StatisticDTO) repo.Find(st => st.InfoId == gameInfo.Id && st.UserId == user.UserId).FirstOrDefault();
        }

        public UserDTO Login(string login, string password)
        {
            IRepository<User> repo = Container.Resolver.Resolve<IRepository<User>>();
            UserDTO dbUser = (UserDTO)repo.Find(u => u.Login.Equals(login)).FirstOrDefault();

            if (dbUser == null)
            {
                return null;
            }

            if (BCrypt.Net.BCrypt.Verify(password, dbUser.Password))
            {
                return dbUser;
            }

            return null;
        }

        public void Register(UserDTO user)
        {
            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password, 10);

            IModificationService<UserDTO> modService = Container.Resolver.Resolve<IModificationService<UserDTO>>();
            modService.CreateItem(user);

            // send email
        }
    }
}
