﻿using System;
using System.Linq;
using BllPart.DI;
using BllPart.DTO;
using Autofac;
using DalPart.Models;
using DalPart.Repositories;

namespace BllPart.Validation.Validators
{
    internal class GameInfoValidator : IValidator<GameInfoDTO>
    {
        public GameInfoValidator() { }

        public bool CheckEntityExisting(int id)
        {
            IRepository<GameInfo> repo = Container.Resolver.Resolve<IRepository<GameInfo>>();
            return repo.Find(info => info.Id == id).Count() > 0;
        }

        public bool ValidateEntity(GameInfoDTO item)
        {
            if (String.IsNullOrWhiteSpace(item.Name) || String.IsNullOrWhiteSpace(item.Description) || String.IsNullOrWhiteSpace(item.Avatar) || String.IsNullOrWhiteSpace(item.ServerName))
            {
                throw new ValidationException(ValidationMistake.Empty_Or_Whitespace_String);
            }

            return true;
        }
    }
}
