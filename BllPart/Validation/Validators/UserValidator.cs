﻿using System.Linq;
using BllPart.DI;
using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using Autofac;
using System;
using System.Text.RegularExpressions;

namespace BllPart.Validation.Validators
{
    internal class UserValidator : IValidator<UserDTO> 
    {
        public UserValidator() { }

        public bool CheckEntityExisting(int id)
        {
            IRepository<User> repo = Container.Resolver.Resolve<IRepository<User>>();
            return repo.Find(u => u.UserId == id).Count() > 0;
        }

        public bool ValidateEntity(UserDTO item)
        {
            // check strings
            if (String.IsNullOrWhiteSpace(item.Avatar) || String.IsNullOrWhiteSpace(item.DisplayName) ||
                String.IsNullOrWhiteSpace(item.Email) || String.IsNullOrWhiteSpace(item.Login) ||
                String.IsNullOrWhiteSpace(item.Password))
            {
                throw new ValidationException(ValidationMistake.Empty_Or_Whitespace_String);
            }

            // check user email
            if (!Regex.IsMatch(item.Email, "[A-Za-z]+@[A-Za-z]+\\.[A-Za-z]+"))
            {
                throw new ValidationException(ValidationMistake.Invalid_Email);
            }

            return true;
        }
    }
}
