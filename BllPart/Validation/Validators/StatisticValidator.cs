﻿using System.Linq;
using BllPart.DI;
using BllPart.DTO;
using DalPart.Models;
using DalPart.Repositories;
using Autofac;

namespace BllPart.Validation.Validators
{
    internal class StatisticValidator : IValidator<StatisticDTO>
    {
        public StatisticValidator() { }

        public bool CheckEntityExisting(int id)
        {
            IRepository<Statistic> repo = Container.Resolver.Resolve<IRepository<Statistic>>();
            return repo.Find(st => st.StatisticId == id).Count() > 0;
        }

        public bool ValidateEntity(StatisticDTO item)
        {
            // check connection to GameInfo
            IRepository<GameInfo> infoRepo = Container.Resolver.Resolve<IRepository<GameInfo>>();

            if (infoRepo.Find(info => info.Id == item.InfoId).Count() == 0)
            {
                throw new ValidationException(ValidationMistake.Related_Entity_Not_Found);
            }

            // check related user
            IRepository<User> userRepo = Container.Resolver.Resolve<IRepository<User>>();

            if (userRepo.Find(u => u.UserId == item.UserId).Count() == 0)
            {
                throw new ValidationException(ValidationMistake.Related_Entity_Not_Found);
            }

            // checking statistic data
            if (item.Computer.Draws < 0 || item.Computer.Looses < 0 || item.Computer.Wins < 0
                || item.Real.Wins < 0 || item.Real.Looses < 0 || item.Real.Draws < 0)
            {
                throw new ValidationException(ValidationMistake.Statistic_Score_Under_Zero);
            }

            return true;
        }
    }
}
