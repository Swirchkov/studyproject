﻿using System.Linq;
using BllPart.DTO;
using DalPart.Repositories;
using DalPart.Models;
using BllPart.DI;
using Autofac;

namespace BllPart.Validation.Validators
{
    internal class GameValidator : IValidator<GameDTO>
    {
        public bool CheckEntityExisting(int id)
        {
            IRepository<Game> repo = Container.Resolver.Resolve<IRepository<Game>>();
            return repo.Find(g => g.GameId == id).Count() > 0;
        }

        public bool ValidateEntity(GameDTO item)
        {
            // check first user existing
            IRepository<User> userRepo = Container.Resolver.Resolve<IRepository<User>>();

            if (userRepo.Find(u => u.UserId == item.User1Id).Count() == 0)
            {
                throw new ValidationException(ValidationMistake.Related_Entity_Not_Found);
            }

            // check second user 
            if (item.User2Id.HasValue && userRepo.Find(u => u.UserId == item.User2Id.Value).Count() == 0)
            {
                throw new ValidationException(ValidationMistake.Related_Entity_Not_Found);
            }

            // check game information object
            IRepository<GameInfo> infoRepo = Container.Resolver.Resolve<IRepository<GameInfo>>();

            if (infoRepo.Find(info => info.Id == item.GameInfoId).Count() == 0)
            {
                throw new ValidationException(ValidationMistake.Related_Entity_Not_Found);
            }


            if (item.FirstScore < 0 && item.SecondScore < 0)
            {
                throw new ValidationException(ValidationMistake.Game_Score_Under_Zero);
            }

            return true;
        }
    }
}
