﻿using System;
using System.Linq;
using BllPart.DTO;
using DalPart.Repositories;
using DalPart.Models;
using BllPart.DI;
using Autofac;

namespace BllPart.Validation.Validators
{
    internal class FeedbackValidator : IValidator<FeedbackDTO>
    {
        public FeedbackValidator() { }

        public bool CheckEntityExisting(int id)
        {
            IRepository<Feedback> repo = Container.Resolver.Resolve<IRepository<Feedback>>();
            return repo.Find(f => f.FeedbackId == id).Count() > 0;
        }

        public bool ValidateEntity(FeedbackDTO item)
        {
            if (String.IsNullOrWhiteSpace(item.Title) || String.IsNullOrWhiteSpace(item.Text))
            {
                throw new ValidationException(ValidationMistake.Empty_Or_Whitespace_String);
            }

            return true;
        }
    }
}
