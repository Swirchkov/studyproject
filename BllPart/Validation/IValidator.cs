﻿namespace BllPart.Validation
{
    internal interface IValidator<T> where T : class
    {
        bool CheckEntityExisting(int id);
        bool ValidateEntity(T item);
    }
}
