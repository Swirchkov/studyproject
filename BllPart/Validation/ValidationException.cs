﻿using System;

namespace BllPart.Validation
{
    public enum ValidationMistake
    {
        Related_Entity_Not_Found,
        Game_Score_Under_Zero,
        Entity_Not_Found,
        Entity_Already_Exists,
        Empty_Or_Whitespace_String,
        Statistic_Score_Under_Zero,
        Invalid_Email
    }
    public class ValidationException : Exception
    {
        public ValidationException() : base() { }

        public ValidationException(string message) : base(message) { }

        public ValidationException(ValidationMistake type)
        {
            ExceptionType = type;
        }

        public ValidationMistake ExceptionType { get; }
    }
}
