﻿using System.Collections.Generic;
using System.Linq;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;

namespace BllPart.DTO
{
    public class UserDTO
    {
        private User user;
        public int UserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Avatar { get; set; }
        public bool IsEmailVerified { get; set; }

        private void loadDomainObject()
        {
            IRepository<User> repo = Container.Resolver.Resolve<IRepository<User>>();
            user = repo.Find(u => u.UserId == this.UserId).FirstOrDefault();
        }

        public static explicit operator User(UserDTO item)
        {
            if (item == null) { return null; }

            return new User
            {
                UserId = item.UserId,
                Login = item.Login,
                Password = item.Password,
                Email = item.Email,
                DisplayName = item.DisplayName,
                Avatar = item.Avatar,
                IsEmailVerified = item.IsEmailVerified
            };
        }

        public static explicit operator UserDTO(User item)
        {
            if (item == null) { return null; }

            return new UserDTO
            {
                UserId = item.UserId,
                Login = item.Login,
                Password = item.Password,
                Email = item.Email,
                DisplayName = item.DisplayName,
                Avatar = item.Avatar,
                IsEmailVerified = item.IsEmailVerified
            };
        }

        public User ToDomainObject()
        {
            return (User)this;
        }

        public IEnumerable<GameDTO> HomeGames
        {
            get
            {
                if (user == null)
                {
                    this.loadDomainObject();
                    if (user == null)
                    {
                        return null;
                    }
                }
                return user.HomeGames.Select(g => (GameDTO)g);
            }
        }

        public IEnumerable<GameDTO> AbroadGames
        {
            get
            {
                if (user == null)
                {
                    this.loadDomainObject();
                    if (user == null)
                    {
                        return null;
                    }
                }
                return user.AbroadGames.Select(g => (GameDTO)g);
            }
        }

        public IEnumerable<StatisticDTO> Statistics
        {
            get
            {
                if (user == null)
                {
                    this.loadDomainObject();
                    if (user == null)
                    {
                        return null;
                    }
                }
                return user.Statistics.Select(st => (StatisticDTO)st);
            }
        }
    }
}
