﻿using System.Linq;
using DalPart.Models;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;

namespace BllPart.DTO
{
    public class GameDTO
    {
        private Game game;

        public int GameId { get; set; }
        public int User1Id { get; set; }
        public int? User2Id { get; set; }
        public int FirstScore { get; set; }
        public int SecondScore { get; set; }
        public int GameInfoId { get; set; }

        private void loadDomainObject()
        {
            IRepository<Game> repo = Container.Resolver.Resolve<IRepository<Game>>();
            game = repo.Find(g => g.GameId == this.GameId).FirstOrDefault();
        }

        public static explicit operator Game(GameDTO item)
        {
            if (item == null) { return null; }

            return new Game()
            {
                GameId = item.GameId,
                User1Id = item.User1Id,
                User2Id = item.User2Id,
                FirstScore = item.FirstScore,
                SecondScore = item.SecondScore,
                GameInfoId = item.GameInfoId
            };
        }

        public static explicit operator GameDTO(Game item)
        {
            if (item == null) { return null; }
            return new GameDTO()
            {
                GameId = item.GameId,
                User1Id = item.User1Id,
                User2Id = item.User2Id,
                FirstScore = item.FirstScore,
                SecondScore = item.SecondScore,
                GameInfoId = item.GameInfoId
            };
        }

        public Game ToDomainObject()
        {
            return (Game)this;
        }

        public UserDTO User1
        {
            get
            {
                if (game == null)
                {
                    this.loadDomainObject();
                    if (game == null)
                    {
                        return null;
                    }
                }
                return (UserDTO)game.User1;
            }
        }

        public UserDTO User2
        {
            get
            {
                if (game == null)
                {
                    this.loadDomainObject();
                    if (game == null)
                    {
                        return null;
                    }
                }
                return (UserDTO)game.User2;
            }
        }

        public GameInfoDTO GameInfo
        {
            get
            {
                if (game == null)
                {
                    this.loadDomainObject();
                    if (game == null)
                    {
                        return null;
                    }
                }
                return (GameInfoDTO)game.GameInfo;
            }
        }
    }
}
