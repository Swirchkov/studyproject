﻿using DalPart.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BllPart.DTO
{
    public class FeedbackDTO
    {
        public int FeedbackId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public bool Processed { get; set; }

        public static explicit operator Feedback(FeedbackDTO item)
        {
            if (item == null) { return null; }

            return new Feedback()
            {
                FeedbackId = item.FeedbackId,
                Title = item.Title,
                Text = item.Text,
                Processed = item.Processed
            };
        }

        public static explicit operator FeedbackDTO(Feedback item)
        {
            if (item == null) { return null; }

            return new FeedbackDTO()
            {
                FeedbackId = item.FeedbackId,
                Title = item.Title,
                Text = item.Text,
                Processed = item.Processed
            };
        }

        public Feedback ToDomainObject()
        {
            return (Feedback)this;
        }
    }
}
