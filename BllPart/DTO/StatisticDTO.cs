﻿using DalPart.Models;
using System.Linq;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;

namespace BllPart.DTO
{
    public class StatisticDTO
    {
        private Statistic statistic;
        public int StatisticId { get; set; }

        public StatisticKeeper Computer { get; set; }
        public StatisticKeeper Real { get; set; }

        public int InfoId { get; set; }
        public int UserId { get; set; }

        private void loadDomainObject()
        {
            IRepository<Statistic> statrepo = Container.Resolver.Resolve<IRepository<Statistic>>();
            statistic = statrepo.Find(st => st.StatisticId == this.StatisticId).FirstOrDefault();
        }

        public static explicit operator Statistic(StatisticDTO item)
        {
            if (item == null) { return null; }

            return new Statistic
            {
                StatisticId = item.StatisticId,
                InfoId = item.InfoId,
                UserId = item.UserId,

                RealDraws = item.Real.Draws,
                RealLoses = item.Real.Looses,
                RealWins = item.Real.Wins,

                WinsII = item.Computer.Wins,
                DrawsII = item.Computer.Draws,
                LosesII = item.Computer.Looses
            };
        }

        public static explicit operator StatisticDTO(Statistic item)
        {
            if (item == null) { return null; }

            return new StatisticDTO
            {
                StatisticId = item.StatisticId,
                InfoId = item.InfoId,
                UserId = item.UserId,

                Computer = new StatisticKeeper
                {
                    Wins = item.WinsII,
                    Draws = item.DrawsII,
                    Looses = item.LosesII
                },

                Real = new StatisticKeeper
                {
                    Wins = item.RealWins,
                    Draws = item.RealDraws,
                    Looses = item.RealLoses
                }
            };
        }

        public Statistic ToDomainObject()
        {
            return (Statistic)this;
        }

        public UserDTO User
        {
            get
            {
                if (statistic == null)
                {
                    this.loadDomainObject();
                    if (statistic == null)
                    {
                        return null;
                    }
                }
                return (UserDTO)statistic.User;
            }
        }

        public GameInfoDTO GameInfo
        {
            get
            {
                if (statistic == null)
                {
                    this.loadDomainObject();
                    if (statistic == null)
                    {
                        return null;
                    }
                }
                return (GameInfoDTO)statistic.Info;
            }
        }

    }
    public class StatisticKeeper
    {
        public int Wins { get; set; }
        public int Draws { get; set; }
        public int Looses { get; set; }

    }
}
