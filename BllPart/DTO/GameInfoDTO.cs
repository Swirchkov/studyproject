﻿using DalPart.Models;
using System.Collections.Generic;
using System.Linq;
using DalPart.Repositories;
using BllPart.DI;
using Autofac;

namespace BllPart.DTO
{
    public class GameInfoDTO
    {
        private GameInfo info;
        public int Id { get; set; }
        public string Name { get; set; }
        public string ServerName { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }

        private void loadDomainObject()
        {
            IRepository<GameInfo> repo = Container.Resolver.Resolve<IRepository<GameInfo>>();
            this.info = repo.Find(info => info.Id == this.Id).FirstOrDefault();
        }

        public static explicit operator GameInfo(GameInfoDTO item)
        {
            if (item == null) { return null; }

            return new GameInfo
            {
                Id = item.Id,
                Name = item.Name,
                ServerName = item.ServerName,
                Description = item.Description,
                Avatar = item.Avatar
            };
        }

        public static explicit operator GameInfoDTO(GameInfo item)
        {
            if (item == null) { return null; }

            return new GameInfoDTO
            {
                Id = item.Id,
                Name = item.Name,
                ServerName = item.ServerName,
                Description = item.Description,
                Avatar = item.Avatar
            };
        }

        public GameInfo ToDomainObject()
        {
            return (GameInfo)this;
        }

        public IEnumerable<StatisticDTO> Statistics
        {
            get
            {
                if (info == null)
                {
                    this.loadDomainObject();
                    if (info == null)
                    {
                        return null;
                    }
                }
                return info.Statistics.Select(st => (StatisticDTO)st);
            }
        }

        public IEnumerable<GameDTO> Games
        {
            get
            {
                if (info == null)
                {
                    this.loadDomainObject();
                    if (info == null)
                    {
                        return null;
                    }
                }
                return info.Games.Select(g => (GameDTO)g);
            }
        }
    }
}
