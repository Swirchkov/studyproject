﻿using Autofac;
using BllPart.Services;
using BllPart.Services.ModificationServices;
using BllPart.DTO;

namespace BllPart.DI
{
    public class BLLModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            // modification services
            builder.RegisterType<FeedbackModificationService>().As<IModificationService<FeedbackDTO>>();
            builder.RegisterType<GameInfoModificationService>().As<IModificationService<GameInfoDTO>>();
            builder.RegisterType<GameModificationService>().As<IModificationService<GameDTO>>();
            builder.RegisterType<StatisticModificationService>().As<IModificationService<StatisticDTO>>();
            builder.RegisterType<UserModificationService>().As<IModificationService<UserDTO>>();

            // main services
            builder.RegisterType<FeedbackService>().As<IFeedbackService>();
            builder.RegisterType<GameService>().As<IGameService>();
            builder.RegisterType<UserService>().As<IUserService>();

            base.Load(builder);
        }

    }
}
