﻿using Autofac;
using DalPart.DI;
using BllPart.DTO;
using BllPart.Validation;
using BllPart.Validation.Validators;
using BllPart.Services.ModificationServices;
using BllPart.Services;

namespace BllPart.DI
{
    internal static class Container
    {
        private static IContainer _container;

        static Container()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterModule<DalModule>();

            // validators
            builder.RegisterType<GameValidator>().As<IValidator<GameDTO>>();
            builder.RegisterType<FeedbackValidator>().As <IValidator<FeedbackDTO>>();
            builder.RegisterType<GameInfoValidator>().As<IValidator<GameInfoDTO>>();
            builder.RegisterType<StatisticValidator>().As<IValidator<StatisticDTO>>();
            builder.RegisterType<UserValidator>().As<IValidator<UserDTO>>();

            // user modification service for user service
            builder.RegisterType<UserModificationService>().As<IModificationService<UserDTO>>();

            _container = builder.Build();
        }

        public static IContainer Resolver
        {
            get
            {
                return _container;
            }
        }

    }
}
